<?php

Route::get('/local/{locale}', 'FrontController@setLang');

Route::get('/', "FrontController@front_index");
Route::get('/all-steps', "FrontController@all_steps");

Route::group(['middleware' => ['auth', 'checkDocAssign']], function () {
    //STEPS
    //STEP 1,2 ROUTES
    Route::get('/project/form/{form_id}', "FrontController@form_generator");
    Route::any('/select2/steps_of_form/{form_id}', 'FrontController@select2FormSteps');
    Route::any('/select/steps_of_form', 'FrontController@selectFormSteps');

    //STEP 6 ROUTES
    Route::get('/project/step/6/new', "StepController@step_6_new");
    Route::post('/project/step_6/create_new', "StepController@step_6_create_new");
    Route::get('/project/step/6/doc/{doc_id}', "StepController@step_6_edit");
    Route::post('/project/step/6/doc/{doc_id}/add', "StepController@step_6_add_subtask");
    Route::post('/project/step/6/doc/{doc_id}/update_name', "StepController@step_6_update_name");
    Route::post('/project/step6/node/', 'StepController@step_6_node_update');
    Route::get('/project/step6/node/{node_id}/delete', 'StepController@step_6_node_delete');
    Route::get('/project/step-6/{doc_id}/delete', 'StepController@step_6_delete_doc');
    Route::get('/download/step-6/{doc_id}', 'StepController@download');

    //STEP 8 ROUTES
    Route::get('/project/step/8/new', "Step8Controller@stepNew");
    Route::get('/project/step/8/edit/{doc_id}', "Step8Controller@stepEdit");
    Route::post('/project/step_8/create_new', "Step8Controller@storeNew");
    Route::post('/project/step/8/save-html', "Step8Controller@saveHtml");
    Route::get('/project/step/8/download/{doc_id}', "Step8Controller@download");
    Route::get('/project/step/8/delete/{doc_id}', "Step8Controller@docDelete");
    Route::post('/project/step/8/doc/{doc_id}/update_name', "Step8Controller@nameUpdate");

    //STEP 12 ROUTES
    Route::get('/project/step/12/new', "Step12Controller@stepNew");
    Route::post('/project/step/12/create-from-doc/{create_from_doc_id}', "Step12Controller@createNew");
    Route::get('/project/step/12/doc/{doc_id}', "Step12Controller@docEdit");
    Route::get('/project/step/12/delete/{doc_id}', "Step12Controller@docDelete");
    Route::get('/project/step/12/get-gantt-json/{doc_id}', "Step12Controller@getGanttJson");
    Route::get('/project/step/12/download-excel/{doc_id}', "Step12Controller@exportExcel");
    Route::post('/project/step/12/doc/{doc_id}/update_name', "Step12Controller@nameUpdate");

    //STEP 14 ROUTES
    Route::get('/project/step/14/new', "Step14Controller@stepNew");
    Route::post('/project/step_14/create_new', "Step14Controller@storeNew");
    Route::get('/project/step/14/edit/{doc_id}', "Step14Controller@docEdit");
    Route::post('/project/step/14/doc/{doc_id}/update_name', "Step14Controller@nameUpdate");
    Route::post('/project/step/14/add-block/{doc_id}', "Step14Controller@addBlock");
    Route::post('/project/step/14/add-statiya/{doc_id}', "Step14Controller@addStatiya");
    Route::post('/project/step/14/ajax-get-statiya', "Step14Controller@getStatiyaEditForm");
    Route::post('/project/step/14/save-statiya/{statiya_id}', "Step14Controller@saveEditedStatiya");
    Route::get('/project/step/14/delete-statiya/{statiya_id}', "Step14Controller@deleteStatiya");
    Route::get('/project/step/14/download/{doc_id}', "Step14Controller@exportExcel");
    Route::get('/project/step/14/delete/{doc_id}', "Step14Controller@delete");

    //STEP 15 ROUTES
    Route::get('/project/step/15/new', "Step15Controller@stepNew");
    Route::post('/project/step_15/create_new', "Step15Controller@storeNew");
    Route::get('/project/step/15/edit/{doc_id}', "Step15Controller@docEdit");
    Route::post('/project/step/15/doc/{doc_id}/update_name', "Step15Controller@nameUpdate");
    Route::post('/project/step/15/add-risk/{doc_id}', "Step15Controller@addRisk");
    Route::post('/project/step/15/ajax-get-risk', "Step15Controller@getRiskEditForm");
    Route::post('/project/step/15/save-risk/{risk_id}', "Step15Controller@saveEditedRisk");
    Route::get('/project/step/15/delete-risk/{risk_id}', "Step15Controller@delete");
    Route::get('/project/step/15/delete/{doc_id}', "Step15Controller@deleteDoc");
    Route::get('/project/step/15/download/{doc_id}', "Step15Controller@exportExcel");

    //STEP 16 ROUTES
    Route::get('/project/step/16/new', "Step16Controller@stepNew");
    Route::post('/project/step/16/create-from-doc/{create_from_doc_id}', "Step16Controller@createNew");
    Route::get('/project/step/16/doc/{doc_id}', "Step16Controller@docEdit");
    Route::post('/project/step/16/doc/{doc_id}/update_name', "Step15Controller@nameUpdate");
    Route::post('/project/step/16/ajax-get-task', "Step16Controller@getTaskEditForm");
    Route::post('/project/step/16/save-task/{task_id}', "Step16Controller@saveEditedTask");
    Route::get('/project/step/16/delete-task/{task_id}', "Step16Controller@delete");
    Route::get('/project/step/16/delete/{doc_id}', "Step16Controller@deleteDoc");
    Route::get('/project/step/16/download/{doc_id}', "Step16Controller@exportExcel");
    Route::post('/project/step/16/add-task/{doc_id}', "Step16Controller@addTask");

    //Gantt Resource Controllers
    Route::resource('/project/step/12/gantt/task', "Step12TasksResourceController");
    Route::resource('/project/step/12/gantt/link', "Step12LinksResourceController");

    Route::get('/cabinet', "FrontController@cabinet");
    Route::get('/mydocs', "FrontController@mydocs");
    Route::post('/project/form/{form_id}/save', "FrontController@form_save");
    Route::get('/mydoc/{doc_id}/download', "FrontController@download");
    Route::post('/update_user_info', "FrontController@update_user_info");
    Route::get('/project/doc/{doc_id}/edit', "FrontController@doc_edit");
    Route::post('/project/doc/{doc_form_id}/save', "FrontController@edited_doc_save");
    Route::get('/mydoc/{doc_id}/delete', "FrontController@delete");
});

Route::get('/page/{page_id}', "FrontController@page");
Route::get('/logout', "Auth\LoginController@logout");

Route::get('/home', function () {
    if (session('status')) {
        return redirect()->route('admin.home')->with('status', session('status'));
    }

    return redirect()->route('admin.home');
});

Auth::routes(['register' => true]);

Route::group(['prefix' => 'admin', 'as' => 'admin.', 'namespace' => 'Admin', 'middleware' => ['auth', 'set_rus_local']], function () {

    Route::get('/adminhome', 'HomeController@index')->name('home');
    // Permissions
    Route::delete('permissions/destroy', 'PermissionsController@massDestroy')->name('permissions.massDestroy');
    Route::resource('permissions', 'PermissionsController');

    // Roles
    Route::delete('roles/destroy', 'RolesController@massDestroy')->name('roles.massDestroy');
    Route::resource('roles', 'RolesController');

    // Users
    Route::delete('users/destroy', 'UsersController@massDestroy')->name('users.massDestroy');
    Route::resource('users', 'UsersController');

    // Forms
    Route::delete('forms/destroy', 'FormsController@massDestroy')->name('forms.massDestroy');
    Route::post('forms/media', 'FormsController@storeMedia')->name('forms.storeMedia');
    Route::post('forms/ckmedia', 'FormsController@storeCKEditorImages')->name('forms.storeCKEditorImages');
    Route::resource('forms', 'FormsController');

    // Steps
    Route::delete('steps/destroy', 'StepsController@massDestroy')->name('steps.massDestroy');
    Route::post('steps/media', 'StepsController@storeMedia')->name('steps.storeMedia');
    Route::post('steps/ckmedia', 'StepsController@storeCKEditorImages')->name('steps.storeCKEditorImages');
    Route::resource('steps', 'StepsController');

    // Fields
    Route::delete('fields/destroy', 'FieldsController@massDestroy')->name('fields.massDestroy');
    Route::resource('fields', 'FieldsController');

    // Pages
    Route::delete('pages/destroy', 'PagesController@massDestroy')->name('pages.massDestroy');
    Route::post('pages/media', 'PagesController@storeMedia')->name('pages.storeMedia');
    Route::post('pages/ckmedia', 'PagesController@storeCKEditorImages')->name('pages.storeCKEditorImages');
    Route::resource('pages', 'PagesController');

    // Gen Docs
    Route::delete('gen-docs/destroy', 'GenDocController@massDestroy')->name('gen-docs.massDestroy');
    Route::resource('gen-docs', 'GenDocController');

    // Answers
    Route::delete('answers/destroy', 'AnswersController@massDestroy')->name('answers.massDestroy');
    Route::resource('answers', 'AnswersController');
});
Route::group(['prefix' => 'profile', 'as' => 'profile.', 'namespace' => 'Auth', 'middleware' => ['auth']], function () {
// Change password
    if (file_exists(app_path('Http/Controllers/Auth/ChangePasswordController.php'))) {
        Route::get('password', 'ChangePasswordController@edit')->name('password.edit');
        Route::post('password', 'ChangePasswordController@update')->name('password.update');
        Route::post('profile', 'ChangePasswordController@updateProfile')->name('password.updateProfile');
        Route::post('profile/destroy', 'ChangePasswordController@destroy')->name('password.destroyProfile');
    }
});
