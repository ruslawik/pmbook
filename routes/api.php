<?php

Route::group(['prefix' => 'v1', 'as' => 'api.', 'namespace' => 'Api\V1\Admin', 'middleware' => ['auth:sanctum']], function () {
    // Permissions
    Route::apiResource('permissions', 'PermissionsApiController');

    // Roles
    Route::apiResource('roles', 'RolesApiController');

    // Users
    Route::apiResource('users', 'UsersApiController');

    // Forms
    Route::post('forms/media', 'FormsApiController@storeMedia')->name('forms.storeMedia');
    Route::apiResource('forms', 'FormsApiController');

    // Steps
    Route::post('steps/media', 'StepsApiController@storeMedia')->name('steps.storeMedia');
    Route::apiResource('steps', 'StepsApiController');

    // Fields
    Route::apiResource('fields', 'FieldsApiController');

    // Pages
    Route::post('pages/media', 'PagesApiController@storeMedia')->name('pages.storeMedia');
    Route::apiResource('pages', 'PagesApiController');

    // Gen Docs
    Route::apiResource('gen-docs', 'GenDocApiController');

    // Answers
    Route::apiResource('answers', 'AnswersApiController');
});
