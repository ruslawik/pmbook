<?php

namespace App\Exports;

use App\Models\Step12Task;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Carbon\Carbon;

class Step12TaskExport implements FromView
{	
	public $gen_doc_id = 0;

	public function __construct($doc_id){
		$this->gen_doc_id = $doc_id;
	}

    public function view(): View
    {
    	//Находим максимальную и минимальную дату в проекте
    	$max_datetime = new Carbon('1996-03-20 00:00:00');
    	$min_datetime = new Carbon('2090-03-20 00:00:00');
    	$all_tasks = Step12Task::where('gen_doc_id', $this->gen_doc_id)->get();
    	foreach ($all_tasks as $task) {
    		$task_datetime = new Carbon($task->start_date);
    		$task__min_datetime = new Carbon($task->start_date);
    		if($task_datetime->addDays($task->duration) > $max_datetime){
    			$max_datetime = $task_datetime;
    		}
    		if($task__min_datetime < $min_datetime){
    			$min_datetime = $task__min_datetime;
    		}
    	}
    	$max_datetime->addDays(62);
    	$min_datetime->subDays(30);

    	$parent_task = Step12Task::where('gen_doc_id', $this->gen_doc_id)
    								->where('parent', 0)->get();
    	$root_tasks = Step12Task::where('parent', $parent_task[0]->id)->orderBy('sortorder')->get();

        return view('exports.step12_gantt', [
            'tasks' => $root_tasks,
            'min_datetime' => $min_datetime,
            'max_datetime' => $max_datetime,
        ]);
    }
}
