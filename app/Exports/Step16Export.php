<?php

namespace App\Exports;

use App\Models\Step16;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Carbon\Carbon;

class Step16Export implements FromView
{	
	public $gen_doc_id = 0;

    public $tasks = array();

	public function __construct($doc_id){
		$this->gen_doc_id = $doc_id;
	}

    //Рекурсивная выборка задач из таблицы step16 (План качества проекта) для правильного отображения дерева
    public function selectStep16Tasks ($root_id){
        $tasks = Step16::where('parent_id', $root_id)->get();
        foreach ($tasks as $task) {
            array_push($this->tasks, $task);
            $this->selectStep16Tasks($task->id);
        }
    }

    public function view(): View
    {
        $select = Step16::where("gen_doc_id", $this->gen_doc_id)->where('parent_id', 0)->get();
        $root_id = $select[0]->id;
        array_push($this->tasks, $select[0]);
        $this->selectStep16Tasks($root_id);

        $ar['tasks'] = $this->tasks;
        
        return view('exports.step16', $ar);
    }
}
