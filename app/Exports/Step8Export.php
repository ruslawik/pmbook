<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Carbon\Carbon;

class Step8Export implements FromView
{	

	public $html_to_export;

	public function __construct($html_to_export){
		$this->html_to_export = $html_to_export;
	}

    public function view(): View
    {
    	return view('exports.step8_export', ["html_to_export" => $this->html_to_export]);
    }
}
