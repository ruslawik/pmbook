<?php

namespace App\Exports;

use App\Models\Step14;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Carbon\Carbon;

class Step14Export implements FromView
{	
	public $gen_doc_id = 0;

	public function __construct($doc_id){
		$this->gen_doc_id = $doc_id;
	}

    public function view(): View
    {
        $ar['blocks'] = Step14::where('gen_doc_id', $this->gen_doc_id)->where('parent_id', 0)->get();
    	
        return view('exports.step14', $ar);
    }
}
