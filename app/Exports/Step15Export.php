<?php

namespace App\Exports;

use App\Models\Step15;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Carbon\Carbon;

class Step15Export implements FromView
{	
	public $gen_doc_id = 0;

	public function __construct($doc_id){
		$this->gen_doc_id = $doc_id;
	}

    public function view(): View
    {
        $ar['risks'] = Step15::where('gen_doc_id', $this->gen_doc_id)->get();
    	$ar['strats'] = array("Избегание", "Передача", "Смягчение", "Принятие");
        
        return view('exports.step15', $ar);
    }
}
