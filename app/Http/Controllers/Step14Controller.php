<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Step14;
use App\Models\GenDoc;
use App\Exports\Step14Export;
use Maatwebsite\Excel\Facades\Excel;
use Auth;

class Step14Controller extends Controller
{
    public function stepNew (){

    	//return Excel::download(new Step8Export(), 'ресурсы.xlsx');
    	//return view("exports.step8");
    	return view("front2.steps.step14.step_new");
    }

    public function exportExcel($doc_id){

        return Excel::download(new Step14Export($doc_id), 'бюджет_проекта.xlsx');
    }

    public function storeNew (Request $r){

    	$gen_doc_name = $r->input("gen_doc_name");

    	$gen_doc = new GenDoc();
    	$form_id = 8;
    	$user_id = Auth::user()->id;
    	$gen_doc->name = $gen_doc_name;
    	$gen_doc->form_id = $form_id;
    	$gen_doc->user_id = $user_id;
    	$gen_doc->save();

    	return redirect("/project/step/14/edit/".$gen_doc->id);
    }

    public function docEdit ($doc_id){

    	$ar['doc'] = GenDoc::where('id', $doc_id)->get();
    	$ar['blocks'] = Step14::where('gen_doc_id', $doc_id)->where('parent_id', 0)->get();
    	
    	return view("front2.steps.step14.step_edit", $ar);
    }

    public function nameUpdate (Request $r, $doc_id){

        GenDoc::where('id', $doc_id)->update(["name" => $r->input("doc_name")]);
        return back()->with('msg', 'Название изменено!');
    }

    public function addBlock (Request $r, $doc_id){

    	$new_block = new Step14();
    	$new_block->statiya_name = $r->input("block_name");
    	$new_block->gen_doc_id = $doc_id;
    	$new_block->parent_id = 0;
    	$new_block->save();

    	return back()->with('msg', 'Блок добавлен!');
    }

    public function addStatiya (Request $r, $doc_id){

    	$new_statiya = new Step14();
    	$new_statiya->statiya_name = $r->input("statiya_name");
    	$new_statiya->price_for_one = $r->input("price_for_one");
    	$new_statiya->time_in_project = $r->input("time_in_project");
    	$new_statiya->total_sum = $r->input("total_sum");
    	$new_statiya->gen_doc_id = $doc_id;
    	$new_statiya->parent_id = $r->input("add_in_block");
    	$new_statiya->save();

    	return back()->with('msg', 'Статья затрат добавлена!');
    }

    public function getStatiyaEditForm (Request $r){

        $statiya_id = $r->input("statiya_id");
        $ar['statiya'] = Step14::where('id', $statiya_id)->get();
        $ar['blocks'] = Step14::where('gen_doc_id', $r->input('doc_id'))->where('parent_id', 0)->get();

        if($ar['statiya'][0]->parent_id == 0){
            return view("front2.steps.step14.edit_block_form", $ar);
        }else{
            return view("front2.steps.step14.edit_row_form", $ar);
        }
    }

    public function saveEditedStatiya (Request $r, $statiya_id){

        Step14::where('id', $statiya_id)
                ->update([
                    "parent_id" => $r->input("add_in_block"),
                    "statiya_name" => $r->input('statiya_name'),
                    "price_for_one" => $r->input('price_for_one'),
                    "time_in_project" => $r->input('time_in_project'),
                    "total_sum" => $r->input('total_sum')
                ]);
        return back()->with('msg', 'Успешно обновлено!');
    }

    public function deleteStatiya ($statiya_id){

        $statiyas = Step14::where('parent_id', $statiya_id)->get();

        foreach ($statiyas as $statiya) {
            Step14::where('id', $statiya->id)->delete();
        }
        
        Step14::where('id', $statiya_id)->delete();
        return back()->with('msg', 'Записи упешно удалены!');
    }

    public function delete ($doc_id){

        Step14::where('gen_doc_id', $doc_id)->delete();
        GenDoc::where('id', $doc_id)->delete();

        return back()->with('msg', 'Документ успешно удален!');
    }

}
