<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreGenDocRequest;
use App\Http\Requests\UpdateGenDocRequest;
use App\Http\Resources\Admin\GenDocResource;
use App\Models\GenDoc;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class GenDocApiController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('gen_doc_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new GenDocResource(GenDoc::with(['form', 'user'])->get());
    }

    public function store(StoreGenDocRequest $request)
    {
        $genDoc = GenDoc::create($request->all());

        return (new GenDocResource($genDoc))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(GenDoc $genDoc)
    {
        abort_if(Gate::denies('gen_doc_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new GenDocResource($genDoc->load(['form', 'user']));
    }

    public function update(UpdateGenDocRequest $request, GenDoc $genDoc)
    {
        $genDoc->update($request->all());

        return (new GenDocResource($genDoc))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function destroy(GenDoc $genDoc)
    {
        abort_if(Gate::denies('gen_doc_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $genDoc->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
