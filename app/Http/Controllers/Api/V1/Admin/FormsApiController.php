<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\StoreFormRequest;
use App\Http\Requests\UpdateFormRequest;
use App\Http\Resources\Admin\FormResource;
use App\Models\Form;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class FormsApiController extends Controller
{
    use MediaUploadingTrait;

    public function index()
    {
        abort_if(Gate::denies('form_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new FormResource(Form::all());
    }

    public function store(StoreFormRequest $request)
    {
        $form = Form::create($request->all());

        if ($request->input('word_template', false)) {
            $form->addMedia(storage_path('tmp/uploads/' . basename($request->input('word_template'))))->toMediaCollection('word_template');
        }

        return (new FormResource($form))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(Form $form)
    {
        abort_if(Gate::denies('form_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new FormResource($form);
    }

    public function update(UpdateFormRequest $request, Form $form)
    {
        $form->update($request->all());

        if ($request->input('word_template', false)) {
            if (!$form->word_template || $request->input('word_template') !== $form->word_template->file_name) {
                if ($form->word_template) {
                    $form->word_template->delete();
                }

                $form->addMedia(storage_path('tmp/uploads/' . basename($request->input('word_template'))))->toMediaCollection('word_template');
            }
        } elseif ($form->word_template) {
            $form->word_template->delete();
        }

        return (new FormResource($form))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function destroy(Form $form)
    {
        abort_if(Gate::denies('form_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $form->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
