<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Exports\Step8Export;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\Step8;
use App\Models\GenDoc;
use Auth;

class Step8Controller extends Controller
{
    public function stepNew (){

    	//return Excel::download(new Step8Export(), 'ресурсы.xlsx');
    	//return view("exports.step8");
    	return view("front2.steps.step8.step_new");
    }

    public function storeNew (Request $r){

    	$gen_doc_name = $r->input("gen_doc_name");

    	$gen_doc = new GenDoc();
    	$form_id = 7;
    	$user_id = Auth::user()->id;
    	$gen_doc->name = $gen_doc_name;
    	$gen_doc->form_id = $form_id;
    	$gen_doc->user_id = $user_id;
    	$gen_doc->save();

    	$step8 = new Step8();
    	$step8->gen_doc_id = $gen_doc->id;
    	$step8->doc_html = view('exports.step8');
    	$step8->save();

    	return redirect("/project/step/8/edit/".$gen_doc->id);
    }

    public function stepEdit ($doc_id){

    	$gen_doc = GenDoc::where('id', $doc_id)->get();
    	$gen_doc_count = GenDoc::where('id', $doc_id)->count();
    	if($gen_doc_count == 1){
    		$user_id = $gen_doc[0]->user_id;
    		if($user_id != Auth::user()->id){
    			die("У Вас нет такого документа.");
    		}
    	}else{
    		die("Такого документа не существует.");
    	}

    	$doc_html_q = Step8::where('gen_doc_id', $doc_id)->get();
    	$ar['doc_html'] = $doc_html_q[0]->doc_html;
    	$ar['doc_name'] = $gen_doc[0]->name;
    	$ar['gen_doc_id'] = $gen_doc[0]->id;

    	return view("front2.steps.step8.step_edit", $ar);
    }

    public function saveHtml (Request $r){
    	$gen_doc_id = $r->input("gen_doc_id");
    	$html = $r->input("html");

    	Step8::where("gen_doc_id", $gen_doc_id)->update(["doc_html" => $html]);
    }

    public function download ($doc_id){

    	$doc = Step8::where('gen_doc_id', $doc_id)->get();
    	$html_to_export = $doc[0]->doc_html;

    	return Excel::download(new Step8Export($html_to_export), 'оценка_ресурсов.xlsx');
    }

    public function nameUpdate (Request $r, $doc_id){

        GenDoc::where('id', $doc_id)->update(["name" => $r->input("doc_name")]);
        return back()->with('msg', 'Название изменено!');
    }

    public function docDelete ($doc_id){

    	GenDoc::where('id', $doc_id)->delete();
		Step8::where('gen_doc_id', $doc_id)->delete();

		return back()->with('msg', 'Документ успешно удален!');
    }
}
