<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\GenDoc;
use App\Models\Step16;
use App\Models\Step6;
use Auth;
use Carbon\Carbon;
use DateTimeZone;
use App\Exports\Step16Export;
use Maatwebsite\Excel\Facades\Excel;

class Step16Controller extends Controller
{

	public $tasks = array();

    public function stepNew (){

    	$ar['all_docs'] = GenDoc::where('user_id', Auth::user()->id)->where('form_id', 4)->get();

    	return view('front2.steps.step16.step_new', $ar);
    }

    public function exportExcel ($doc_id){

        return Excel::download(new Step16Export($doc_id), 'план_качества_проекта.xlsx');
    }

    public function docEdit ($doc_id){

    	$ar['doc_id'] = $doc_id;
        $ar['doc'] = GenDoc::where('id', $doc_id)->get();
		$select = Step16::where("gen_doc_id", $doc_id)->where('parent_id', 0)->get();
		$root_id = $select[0]->id;
		array_push($this->tasks, $select[0]);
        $this->selectStep16Tasks($root_id);

        $ar['tasks'] = $this->tasks;

    	return view('front2.steps.step16.step_edit', $ar);
    }

    public function getTaskEditForm (Request $r){

    	$ar['task'] = Step16::where('id', $r->input('task_id'))->get();
    	$ar['tasks'] = Step16::where('gen_doc_id', $r->input('gen_doc_id'))->get();

    	return view("front2.steps.step16.edit_task_form", $ar);
    }

    public function delete ($task_id){

    	Step16::where('id', $task_id)->delete();
    	Step16::where('parent_id', $task_id)->delete();
    	return back()->with('msg', 'Задача и ее подзадачи успешно удалены!');	
    }

    public function addTask(Request $r){
    	Step16::insert([
    				"task_text" => $r->input('task_text'),
			    	"pokaz" => $r->input('pokaz'),
			    	"pokaz_ed_izm" => $r->input('pokaz_ed_izm'),
			    	"criteria" => $r->input('criteria'),
			    	"goal_pokaz" => $r->input('goal_pokaz'),
			    	"goal_pokaz_ed_izm" => $r->input('goal_pokaz_ed_izm'),
			    	"parent_id" => $r->input('parent_id')
    			]);

    	return back()->with('msg', 'Задача успешно добавлена!');
    }

    public function saveEditedTask (Request $r, $task_id){

    	Step16::where('id', $task_id)
    			->update([
    				"task_text" => $r->input('task_text'),
			    	"pokaz" => $r->input('pokaz'),
			    	"pokaz_ed_izm" => $r->input('pokaz_ed_izm'),
			    	"criteria" => $r->input('criteria'),
			    	"goal_pokaz" => $r->input('goal_pokaz'),
			    	"goal_pokaz_ed_izm" => $r->input('goal_pokaz_ed_izm'),
			    	"parent_id" => $r->input('parent_id')
    			]);

    	return back()->with('msg', 'Задача успешно обновлена!');
    }

    public function nameUpdate (Request $r, $doc_id){

        GenDoc::where('id', $doc_id)->update(["name" => $r->input("doc_name")]);
        return back()->with('msg', 'Название изменено!');
    }

    public function createNew ($create_from_doc_id, Request $r){

    	$name = $r->input('gen_doc_name');
    	$form_id = 10;
    	$user_id = Auth::user()->id;

    	$gen_doc = new GenDoc();
    	$gen_doc->name = $name;
    	$gen_doc->form_id = $form_id;
    	$gen_doc->user_id = $user_id;
    	$gen_doc->save();

    	$root_task = Step6::where('gen_doc_id', $create_from_doc_id)->where('parent_id', 0)->get();
    	
    	$step16_task = new Step16();
		$step16_task->task_text = $root_task[0]->name;
		$step16_task->created_at = Carbon::now(new DateTimeZone('Asia/Almaty'));
		$step16_task->parent_id = 0;
		$step16_task->gen_doc_id = $gen_doc->id;
		$step16_task->save();

    	$this->insertStep16Tasks($root_task[0]->id, $step16_task->id, $gen_doc->id);

    	return redirect("/project/step/16/doc/".$gen_doc->id)->with("msg", "Документ успешно создан!");

    }
    //Рекурсивная вставка задач из таблицы step6 (иерархия задач) в таблицу step16 (План качества проекта)
    public function insertStep16Tasks ($step6_task_id, $parent_id, $gen_doc_id){
    	$tasks = Step6::where('parent_id', $step6_task_id)->get();
    	foreach ($tasks as $task) {
    		$step16_task = new Step16();
			$step16_task->task_text = $task->name;
			$step16_task->created_at = Carbon::now(new DateTimeZone('Asia/Almaty'));
			$step16_task->parent_id = $parent_id;
			$step16_task->gen_doc_id = $gen_doc_id;
			$step16_task->save();
    		$this->insertStep16Tasks($task->id, $step16_task->id, $gen_doc_id);
    	}
    }

    //Рекурсивная выборка задач из таблицы step16 (План качества проекта) для правильного отображения дерева
    public function selectStep16Tasks ($root_id){
    	$tasks = Step16::where('parent_id', $root_id)->get();
    	foreach ($tasks as $task) {
    		array_push($this->tasks, $task);
    		$this->selectStep16Tasks($task->id);
    	}
    }

    public function deleteDoc ($doc_id){
    	Step16::where('gen_doc_id', $doc_id)->delete();
    	GenDoc::where('id', $doc_id)->delete();
    	return back()->with('msg', 'Документ успешно удален!');
    }

}
