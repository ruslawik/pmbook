<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyGenDocRequest;
use App\Http\Requests\StoreGenDocRequest;
use App\Http\Requests\UpdateGenDocRequest;
use App\Models\Form;
use App\Models\GenDoc;
use App\Models\User;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class GenDocController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('gen_doc_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $genDocs = GenDoc::with(['form', 'user'])->get();

        $forms = Form::get();

        $users = User::get();

        return view('admin.genDocs.index', compact('genDocs', 'forms', 'users'));
    }

    public function create()
    {
        abort_if(Gate::denies('gen_doc_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $forms = Form::all()->pluck('form_name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $users = User::all()->pluck('email', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.genDocs.create', compact('forms', 'users'));
    }

    public function store(StoreGenDocRequest $request)
    {
        $genDoc = GenDoc::create($request->all());

        return redirect()->route('admin.gen-docs.index');
    }

    public function edit(GenDoc $genDoc)
    {
        abort_if(Gate::denies('gen_doc_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $forms = Form::all()->pluck('form_name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $users = User::all()->pluck('email', 'id')->prepend(trans('global.pleaseSelect'), '');

        $genDoc->load('form', 'user');

        return view('admin.genDocs.edit', compact('forms', 'users', 'genDoc'));
    }

    public function update(UpdateGenDocRequest $request, GenDoc $genDoc)
    {
        $genDoc->update($request->all());

        return redirect()->route('admin.gen-docs.index');
    }

    public function show(GenDoc $genDoc)
    {
        abort_if(Gate::denies('gen_doc_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $genDoc->load('form', 'user', 'genDocAnswers');

        return view('admin.genDocs.show', compact('genDoc'));
    }

    public function destroy(GenDoc $genDoc)
    {
        abort_if(Gate::denies('gen_doc_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $genDoc->delete();

        return back();
    }

    public function massDestroy(MassDestroyGenDocRequest $request)
    {
        GenDoc::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
