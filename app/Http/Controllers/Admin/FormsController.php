<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\MassDestroyFormRequest;
use App\Http\Requests\StoreFormRequest;
use App\Http\Requests\UpdateFormRequest;
use App\Models\Form;
use Gate;
use Illuminate\Http\Request;
use Spatie\MediaLibrary\Models\Media;
use Symfony\Component\HttpFoundation\Response;

class FormsController extends Controller
{
    use MediaUploadingTrait;

    public function index()
    {
        abort_if(Gate::denies('form_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $forms = Form::with(['media'])->get();

        return view('admin.forms.index', compact('forms'));
    }

    public function create()
    {
        abort_if(Gate::denies('form_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.forms.create');
    }

    public function store(StoreFormRequest $request)
    {
        $form = Form::create($request->all());

        $form->setTranslation('form_name', 'ru', $request->input('form_name'));
        $form->setTranslation('form_name', 'en', $request->input('form_name_en'));
        $form->setTranslation('form_name', 'kz', $request->input('form_name_kz'));
        $form->save();

        if ($request->input('word_template', false)) {
            $form->addMedia(storage_path('tmp/uploads/' . basename($request->input('word_template'))))->toMediaCollection('word_template');
        }

        if ($media = $request->input('ck-media', false)) {
            Media::whereIn('id', $media)->update(['model_id' => $form->id]);
        }

        return redirect()->route('admin.forms.index');
    }

    public function edit(Form $form)
    {
        abort_if(Gate::denies('form_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.forms.edit', compact('form'));
    }

    public function update(UpdateFormRequest $request, Form $form)
    {
        $form->update($request->all());

        $form->setTranslation('form_name', 'ru', $request->input('form_name'));
        $form->setTranslation('form_name', 'en', $request->input('form_name_en'));
        $form->setTranslation('form_name', 'kz', $request->input('form_name_kz'));
        $form->save();

        if ($request->input('word_template', false)) {
            if (!$form->word_template || $request->input('word_template') !== $form->word_template->file_name) {
                if ($form->word_template) {
                    $form->word_template->delete();
                }

                $form->addMedia(storage_path('tmp/uploads/' . basename($request->input('word_template'))))->toMediaCollection('word_template');
            }
        } elseif ($form->word_template) {
            $form->word_template->delete();
        }

        return redirect()->route('admin.forms.index');
    }

    public function show(Form $form)
    {
        abort_if(Gate::denies('form_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $form->load('formFields', 'formSteps');

        return view('admin.forms.show', compact('form'));
    }

    public function destroy(Form $form)
    {
        abort_if(Gate::denies('form_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $form->delete();

        return back();
    }

    public function massDestroy(MassDestroyFormRequest $request)
    {
        Form::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }

    public function storeCKEditorImages(Request $request)
    {
        abort_if(Gate::denies('form_create') && Gate::denies('form_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $model         = new Form();
        $model->id     = $request->input('crud_id', 0);
        $model->exists = true;
        $media         = $model->addMediaFromRequest('upload')->toMediaCollection('ck-media');

        return response()->json(['id' => $media->id, 'url' => $media->getUrl()], Response::HTTP_CREATED);
    }
}
