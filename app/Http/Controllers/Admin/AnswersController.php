<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyAnswerRequest;
use App\Http\Requests\StoreAnswerRequest;
use App\Http\Requests\UpdateAnswerRequest;
use App\Models\Answer;
use App\Models\Field;
use App\Models\GenDoc;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class AnswersController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('answer_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $answers = Answer::with(['field', 'gen_doc'])->get();

        $fields = Field::get();

        $gen_docs = GenDoc::get();

        return view('admin.answers.index', compact('answers', 'fields', 'gen_docs'));
    }

    public function create()
    {
        abort_if(Gate::denies('answer_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $fields = Field::all()->pluck('user_name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $gen_docs = GenDoc::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.answers.create', compact('fields', 'gen_docs'));
    }

    public function store(StoreAnswerRequest $request)
    {
        $answer = Answer::create($request->all());

        return redirect()->route('admin.answers.index');
    }

    public function edit(Answer $answer)
    {
        abort_if(Gate::denies('answer_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $fields = Field::all()->pluck('user_name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $gen_docs = GenDoc::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $answer->load('field', 'gen_doc');

        return view('admin.answers.edit', compact('fields', 'gen_docs', 'answer'));
    }

    public function update(UpdateAnswerRequest $request, Answer $answer)
    {
        $answer->update($request->all());

        return redirect()->route('admin.answers.index');
    }

    public function show(Answer $answer)
    {
        abort_if(Gate::denies('answer_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $answer->load('field', 'gen_doc');

        return view('admin.answers.show', compact('answer'));
    }

    public function destroy(Answer $answer)
    {
        abort_if(Gate::denies('answer_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $answer->delete();

        return back();
    }

    public function massDestroy(MassDestroyAnswerRequest $request)
    {
        Answer::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
