<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\MassDestroyStepRequest;
use App\Http\Requests\StoreStepRequest;
use App\Http\Requests\UpdateStepRequest;
use App\Models\Form;
use App\Models\Step;
use Gate;
use Illuminate\Http\Request;
use Spatie\MediaLibrary\Models\Media;
use Symfony\Component\HttpFoundation\Response;

class StepsController extends Controller
{
    use MediaUploadingTrait;

    public function index()
    {
        abort_if(Gate::denies('step_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $steps = Step::with(['form'])->get();

        $forms = Form::get();

        return view('admin.steps.index', compact('steps', 'forms'));
    }

    public function create()
    {
        abort_if(Gate::denies('step_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $forms = Form::all()->pluck('form_name', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.steps.create', compact('forms'));
    }

    public function store(StoreStepRequest $request)
    {
        $step = Step::create($request->all());

        $step->setTranslation('name', 'ru', $request->input('name'));
        $step->setTranslation('name', 'en', $request->input('name_en'));
        $step->setTranslation('name', 'kz', $request->input('name_kz'));
        $step->save();

        if ($media = $request->input('ck-media', false)) {
            Media::whereIn('id', $media)->update(['model_id' => $step->id]);
        }

        return redirect()->route('admin.steps.index');
    }

    public function edit(Step $step)
    {
        abort_if(Gate::denies('step_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $forms = Form::all()->pluck('form_name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $step->load('form');

        return view('admin.steps.edit', compact('forms', 'step'));
    }

    public function update(UpdateStepRequest $request, Step $step)
    {
        $step->update($request->all());

        $step->setTranslation('name', 'ru', $request->input('name'));
        $step->setTranslation('name', 'en', $request->input('name_en'));
        $step->setTranslation('name', 'kz', $request->input('name_kz'));
        $step->save();

        return redirect()->route('admin.steps.index');
    }

    public function show(Step $step)
    {
        abort_if(Gate::denies('step_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $step->load('form', 'stepFields');

        return view('admin.steps.show', compact('step'));
    }

    public function destroy(Step $step)
    {
        abort_if(Gate::denies('step_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $step->delete();

        return back();
    }

    public function massDestroy(MassDestroyStepRequest $request)
    {
        Step::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }

    public function storeCKEditorImages(Request $request)
    {
        abort_if(Gate::denies('step_create') && Gate::denies('step_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $model         = new Step();
        $model->id     = $request->input('crud_id', 0);
        $model->exists = true;
        $media         = $model->addMediaFromRequest('upload')->toMediaCollection('ck-media');

        return response()->json(['id' => $media->id, 'url' => $media->getUrl()], Response::HTTP_CREATED);
    }
}
