<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyFieldRequest;
use App\Http\Requests\StoreFieldRequest;
use App\Http\Requests\UpdateFieldRequest;
use App\Models\Field;
use App\Models\Form;
use App\Models\Step;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class FieldsController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('field_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $fields = Field::with(['form', 'step'])->get();

        $forms = Form::get();

        $steps = Step::get();

        return view('admin.fields.index', compact('fields', 'forms', 'steps'));
    }

    public function create()
    {
        abort_if(Gate::denies('field_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $forms = Form::all()->pluck('form_name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $steps = Step::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.fields.create', compact('forms', 'steps'));
    }

    public function store(StoreFieldRequest $request)
    {
        $field = Field::create($request->all());

        $field->setTranslation('user_name', 'ru', $request->input('user_name'));
        $field->setTranslation('user_name', 'en', $request->input('user_name_en'));
        $field->setTranslation('user_name', 'kz', $request->input('user_name_kz'));
        $field->save();

        return redirect()->route('admin.fields.index');
    }

    public function edit(Field $field)
    {
        abort_if(Gate::denies('field_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $forms = Form::all()->pluck('form_name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $steps = Step::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $field->load('form', 'step');

        return view('admin.fields.edit', compact('forms', 'steps', 'field'));
    }

    public function update(UpdateFieldRequest $request, Field $field)
    {
        $field->update($request->all());

        $field->setTranslation('user_name', 'ru', $request->input('user_name'));
        $field->setTranslation('user_name', 'en', $request->input('user_name_en'));
        $field->setTranslation('user_name', 'kz', $request->input('user_name_kz'));
        $field->save();

        return redirect()->route('admin.fields.index');
    }

    public function show(Field $field)
    {
        abort_if(Gate::denies('field_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $field->load('form', 'step');

        return view('admin.fields.show', compact('field'));
    }

    public function destroy(Field $field)
    {
        abort_if(Gate::denies('field_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $field->delete();

        return back();
    }

    public function massDestroy(MassDestroyFieldRequest $request)
    {
        Field::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
