<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Step15;
use App\Models\GenDoc;
use App\Exports\Step15Export;
use Maatwebsite\Excel\Facades\Excel;
use Auth;

class Step15Controller extends Controller
{
    public function stepNew (){

    	//return Excel::download(new Step8Export(), 'ресурсы.xlsx');
    	//return view("exports.step8");
    	return view("front2.steps.step15.step_new");
    }

    public function exportExcel ($doc_id){

        return Excel::download(new Step15Export($doc_id), 'определение_рисков.xlsx');
    }

    public function storeNew (Request $r){

    	$gen_doc_name = $r->input("gen_doc_name");

    	$gen_doc = new GenDoc();
    	$form_id = 9;
    	$user_id = Auth::user()->id;
    	$gen_doc->name = $gen_doc_name;
    	$gen_doc->form_id = $form_id;
    	$gen_doc->user_id = $user_id;
    	$gen_doc->save();

    	return redirect("/project/step/15/edit/".$gen_doc->id);
    }

    public function docEdit ($doc_id){

    	$ar['doc'] = GenDoc::where('id', $doc_id)->get();
    	$ar['risks'] = Step15::where('gen_doc_id', $doc_id)->get();
    	$ar['strats'] = array("Избегание", "Передача", "Смягчение", "Принятие");

    	return view("front2.steps.step15.step_edit", $ar);
    }

    public function nameUpdate (Request $r, $doc_id){

        GenDoc::where('id', $doc_id)->update(["name" => $r->input("doc_name")]);
        return back()->with('msg', 'Название изменено!');
    }

    public function addRisk (Request $r, $doc_id){

    	$risk = new Step15();
    	$risk->gen_doc_id = $doc_id;
    	$risk->name = $r->input('name');
    	$risk->ver = $r->input('ver');
    	$risk->ur = $r->input('ur');
    	$risk->strat = $r->input('strat');
    	$risk->takt = $r->input('takt');
    	$risk->save();

    	return back()->with('msg', 'Риск успешно добавлен!');
    }

    public function getRiskEditForm (Request $r){

    	$ar['risk'] = Step15::where('id', $r->input('risk_id'))->get();
    	return view("front2.steps.step15.edit_risk_form", $ar);
    }	

    public function saveEditedRisk (Request $r, $risk_id){

    	Step15::where('id', $risk_id)
    			->update([
    				"name" => $r->input('name'),
			    	"ver" => $r->input('ver'),
			    	"ur" => $r->input('ur'),
			    	"strat" => $r->input('strat'),
			    	"takt" => $r->input('takt')
    			]);

    	return back()->with('msg', 'Риск успешно обновлен!');
    }

    public function delete ($risk_id){

    	Step15::where('id', $risk_id)->delete();
    	return back()->with('msg', 'Риск успешно удален!');	
    }

    public function deleteDoc ($doc_id){
    	Step15::where('gen_doc_id', $doc_id)->delete();
    	GenDoc::where('id', $doc_id)->delete();
    	return back()->with('msg', 'Документ успешно удален!');
    }
}
