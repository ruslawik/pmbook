<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Models\GenDoc;
use App\Models\Step6;

use PhpOffice\PhpWord;

class StepController extends Controller
{

	public $tree_html = "";
	public $tree_html2 = "";

    public function step_6_new(){


    	return view("front2.step_6_new");
    }

    public function step_6_create_new(Request $r){
    	$name = $r->input('gen_doc_name');
    	$form_id = 4;
    	$user_id = Auth::user()->id;

    	$gen_doc = new GenDoc();
    	$gen_doc->name = $name;
    	$gen_doc->form_id = $form_id;
    	$gen_doc->user_id = $user_id;
    	$gen_doc->save();

    	$step_6  = new Step6();
    	$step_6->name = "Результат проекта";
    	$step_6->parent_id = 0;
    	$step_6->gen_doc_id = $gen_doc->id;
    	$step_6->save();

    	return redirect("/project/step/6/doc/".$gen_doc->id)->with("msg", "Документ успешно создан!");
    }

    public function category_tree($catid, $gen_doc_id){
 	
		$rows = Step6::where('gen_doc_id', $gen_doc_id)
					  ->where('parent_id', $catid)
					  ->get();

		if($catid!=0){
			$this->tree_html .= '<ul>';
		}
		foreach ($rows as $row) {
 			$this->tree_html .=  '<li><span id="node'.$row->id.'" onClick="edit('.$row->id.');">' . $row->name.'</span>';
			$this->category_tree($row->id, $gen_doc_id);
 			$this->tree_html .= '</li>';
 			$this->tree_html .= '</ul>';
		}
	}

	public function category_tree2($catid, $gen_doc_id){
 	
		$rows = Step6::where('gen_doc_id', $gen_doc_id)
					  ->where('parent_id', $catid)
					  ->get();

		foreach ($rows as $row) {
 			$i = 0;
			if ($i == 0) $this->tree_html2 .= '<ul>';
 			$this->tree_html2 .= '<li>' . $row->name;
 			$this->category_tree2($row->id, $gen_doc_id);
 			$this->tree_html2 .= '</li>';
			$i++;
 			if ($i > 0) $this->tree_html2 .= '</ul>';
		}
	}

    public function step_6_edit($doc_id){

    	$gen_doc = GenDoc::where('id', $doc_id)->get();
    	$root_task = Step6::whereNull('parent_id')->where('gen_doc_id', $doc_id)->get();
    	$all_tasks = Step6::where('gen_doc_id', $doc_id)->get();

    	$this->category_tree(0, $doc_id);
    	$this->tree_html = preg_replace("/<ul><\/li><\/ul>/", "</li>", $this->tree_html);

    	return view("front2.step_6_edit", ["doc" => $gen_doc, "tree_html"=>$this->tree_html, "all_tasks"=>$all_tasks]);
    }

    public function step_6_add_subtask(Request $r){

    	$name = $r->input('subtask');
    	$parent_id = $r->input('root_element');
    	$gen_doc_id = $r->input('gen_doc_id');

    	$task = new Step6();
    	$task->name = $name;
    	$task->parent_id = $parent_id;
    	$task->gen_doc_id =  $gen_doc_id;
    	$task->save();

    	return redirect('/project/step/6/doc/'.$gen_doc_id.'?root='.$parent_id);
    }

    public function step_6_node_update(Request $r){

    	Step6::where('id', $r->input('node_id'))->update(['name'=>$r->input('node_text')]);

    	return back()->with('msg', 'Успешно обновлено!');

    }

    public function step_6_node_delete($node_id){

    	$step_data = Step6::where('id', $node_id)->get();
    	if($step_data[0]->parent_id!=0){
    		Step6::where('id', $node_id)->delete();
    		$rows = Step6::where('parent_id', $node_id)->get();
    		foreach ($rows as $row) {
    			$this->step_6_node_delete($row->id);
    		}
    	}

    	return back();
    }

    public function step_6_delete_doc($doc_id){

    	$gen_doc = GenDoc::where('id', $doc_id)->get();

    	if($gen_doc[0]->user_id != Auth::user()->id){
    		die("Невозможно удалить чужой документ.");
    	}  

    	GenDoc::where('id', $doc_id)->delete();
    	Step6::where('gen_doc_id', $doc_id)->delete();

    	return back()->with('msg', 'Документ успешно удален!');
    }

    public function step_6_update_name($doc_id, Request $r){

    	GenDoc::where('id', $doc_id)->update(["name"=>$r->input('doc_name')]);
    	return back();
    }

    public function download($doc_id){

    	$doc = GenDoc::where('id', $doc_id)->get();

    	$phpWord = new \PhpOffice\PhpWord\PhpWord();
        $section = $phpWord->addSection();
        $html = '<h1>Создание иерархической структуры работ по проекту "'.$doc[0]->name.'"</h1>';

        $this->category_tree2(0, $doc_id);
    	//$this->tree_html = preg_replace("/<ul><\/li><\/ul>/", "</li>", $this->tree_html);

    	$html .= $this->tree_html2;

        \PhpOffice\PhpWord\Shared\Html::addHtml($section, $html, false, false);

        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter(
            $phpWord, 'Word2007', $download = true
        );

       	header("Content-Disposition: attachment; filename=".$doc[0]->name.".docx");

		$objWriter->save("php://output");

    }
}
