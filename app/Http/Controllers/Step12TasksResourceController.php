<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Step12Task;

class Step12TasksResourceController extends Controller
{
    public function store(Request $request){
		$task = new Step12Task();

		$task->text = $request->text;
		$task->start_date = $request->start_date;
		$task->duration = $request->duration;
		$task->progress = $request->has("progress") ? $request->progress : 0;
		$task->parent = $request->parent;
		$task->gen_doc_id = $request->gen_doc_id;
		$task->sortorder = Step12Task::max("sortorder") + 1;;

		$task->save();

		return response()->json([
			"action"=> "inserted",
			"tid" => $task->id
		]);
	}

	public function update($id, Request $request){
		$task = Step12Task::find($id);

		$task->text = $request->text;
		$task->start_date = $request->start_date;
		$task->duration = $request->duration;
		$task->progress = $request->has("progress") ? $request->progress : 0;
		$task->parent = $request->parent;
		$task->gen_doc_id = $request->gen_doc_id;
		
		if($request->has("target")){
			$this->updateOrder($id, $request->target);
		}

		$task->save();

		return response()->json([
			"action"=> "updated"
		]);
	}

	private function updateOrder($taskId, $target){
		$nextTask = false;
		$targetId = $target;

		if(strpos($target, "next:") === 0){
			$targetId = substr($target, strlen("next:"));
			$nextTask = true;
		}

		if($targetId == "null")
			return;

		$targetOrder = Step12Task::find($targetId)->sortorder;
		if($nextTask)
			$targetOrder++;

		Step12Task::where("sortorder", ">=", $targetOrder)->increment("sortorder");

		$updatedTask = Step12Task::find($taskId);
		$updatedTask->sortorder = $targetOrder;
		$updatedTask->save();
	}


	public function destroy($id){
		$task = Step12Task::find($id);
		$task->delete();

		return response()->json([
			"action"=> "deleted"
		]);
	}
}
