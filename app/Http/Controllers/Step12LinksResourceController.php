<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Step12Link;

class Step12LinksResourceController extends Controller
{
    public function store(Request $request){

		$link = new Step12Link();

		$link->type = $request->type;
		$link->source = $request->source;
		$link->target = $request->target;
		$link->gen_doc_id = $request->gen_doc_id;

		$link->save();

		return response()->json([
			"action"=> "inserted",
			"tid" => $link->id
		]);
	}

	public function update($id, Request $request){
		$link = Step12Link::find($id);

		$link->type = $request->type;
		$link->source = $request->source;
		$link->target = $request->target;
		$link->gen_doc_id = $request->gen_doc_id;

		$link->save();

		return response()->json([
			"action"=> "updated"
		]);
	}

	public function destroy($id){
		$link = Step12Link::find($id);
		$link->delete();

		return response()->json([
			"action"=> "deleted"
		]);
	}
}
