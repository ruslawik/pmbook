<?php

namespace App\Http\Controllers;
use App\Models\Form;
use App\Models\Step;
use App\Models\Field;
use App\Models\User;
use App\Models\GenDoc;
use App\Models\Answer;
use App\Models\Page;
use Response;
use Auth;
use Hash;

use PhpOffice\PhpWord;
use Illuminate\Http\Request;

class FrontController extends Controller
{

    public function setLang($locale){
        app()->setlocale($locale);
        session()->put('language', $locale);
        return redirect()->back();
    }

    public function select2FormSteps($form_id){

        $data = Step::where('form_id', $form_id)->get();
        foreach ($data as $step) {
            $select2[] = array("id"=>$step->id, "text"=> $step->name);
        }
        echo json_encode($select2);
    }

    public function selectFormSteps(Request $r){

        $form = Form::where('form_name->ru', $r->input('name'))->get();
        $data = Step::where('form_id', $form[0]->id)->get();
        print("<option value>Все</option>");
        foreach ($data as $step) {
            print("<option value=".$step->name.">".$step->name."</option>");
        }
    }

    function front_index(){

        $page = Page::where('id', 12)->get();

        return view("front.index", ["page" => $page]);
    }

    function page($page_id){

    	$page = Page::where('id', $page_id)->get();

        preg_match('/(https\:\/\/www\.youtube\.com\/watch\?v=(.*?))\"/', $page[0]->content, $out);
        if(count($out)>0){
            $page[0]->content = preg_replace("/\<figure class=\"media\"\>\<oembed url\=(.*?)\>\<\/oembed\>\<\/figure\>/", '<iframe width="560" height="315" src="https://www.youtube.com/embed/'.$out[2].'" frameborder="0" allowfullscreen></iframe>', $page[0]->content);
        }

    	return view("front.page", ["page" => $page]);
    }

    function all_steps(){


        return view("front2.all_steps");
    }

    function form_generator($form_id){

        $form = Form::where('id', $form_id)->get();

        return view("front.form", ['form' => $form]);
        
    }

    function doc_edit($doc_id){

    	$gen_doc = GenDoc::where('id', $doc_id)->get();
    	$form = Form::where('id', $gen_doc[0]->form_id)->get();
    	$answers = Answer::where('gen_doc_id', $doc_id)->get();

		if($gen_doc[0]->user_id != Auth::user()->id){
    		die("Невозможно редактировать чужой документ.");
    	}    	

    	return view("front.doc_edit", ["gen_doc" => $gen_doc, "answers" => $answers, "form"=>$form]);
    }

    function edited_doc_save(Request $r){

    	$form = Form::where('id', $r->input('form_id'))->get();

    	$gen_doc = GenDoc::where('id', $r->input('gen_doc_id'))->get();

    	if($gen_doc[0]->user_id != Auth::user()->id){
    		die("Невозможно редактировать чужой документ.");
    	}  

    	$k=0;
    	$first_ans = "";
    	foreach($r->all() as $key => $value) {
  			if($k==3){
  				$first_ans = $value;
  			}
    		$k++;
		}

		$gen_doc_name = substr($first_ans, 0, 252)."...";
		GenDoc::where('id', $r->input('gen_doc_id'))->update(["name"=>$gen_doc_name]);

		foreach($r->all() as $key => $value) {
    		$all_fields_data = Form::where('id', $form[0]->id)->get();
    		$all_fields = $all_fields_data[0]->formFields;
    		foreach ($all_fields as $field) {
    			if($field->name == $key){
    				Answer::where("id", $r->input($key."_answer_id"))->update(["answer"=>$value]);
    			}
    		}
		}

    	return back()->with('msg', 'Документ успешно сохранен.');
    }

    function form_save(Request $r){

    	$form = Form::where('id', $r->input('form_id'))->get();

    	$k=0;
    	$first_ans = "";
    	foreach($r->all() as $key => $value) {
  			if($k==2){
  				$first_ans = $value;
  			}
    		$k++;
		}

		$gen_doc = new GenDoc();
		$gen_doc->name = substr($first_ans, 0, 252)."...";
		$gen_doc->form_id = $form[0]->id;
		$gen_doc->user_id = Auth::user()->id;
		$gen_doc->save();
		$saved_doc_id = $gen_doc->id;

		foreach($r->all() as $key => $value) {
    		$all_fields_data = Form::where('id', $form[0]->id)->get();
    		$all_fields = $all_fields_data[0]->formFields;
    		foreach ($all_fields as $field) {
    			if($field->name == $key){
    				$ans = new Answer();
    				$ans->answer = $value;
    				$ans->field_id = $field->id;
    				$ans->gen_doc_id = $saved_doc_id;
    				$ans->save();
    			}
    		}
		}

		return redirect('/mydocs')->with('msg', 'Документ успешно сохранен <a href="/mydoc/'.$saved_doc_id.'/download"><button class="btn btn-success">Скачать</button></a>');
    }

    public function download($doc_id){

    	$answers = Answer::where('gen_doc_id', $doc_id)->get();
    	$gen_doc_info = GenDoc::where('id', $doc_id)->get();
    	$form = Form::where('id', $gen_doc_info[0]->form_id)->get();

    	if($gen_doc_info[0]->user_id != Auth::user()->id){
    		die("Невозможно скачать чужой документ.");
    	}

    	$template = new\PhpOffice\PhpWord\TemplateProcessor($form[0]->word_template->getPath());

    	foreach ($answers as $answer) {
    		$field_data = Field::where('id', $answer->field_id)->get();
    		$template->setValue($field_data[0]->name, $answer->answer);
    	}

    	header("Content-Disposition: attachment; filename=".$form[0]->form_name.".docx");
  		$template->saveAs('php://output');
		//$template->saveAs('../public/'.$saved_doc_id.'.docx');
    }

    public function delete($doc_id){

    	$gen_doc = GenDoc::where('id', $doc_id)->get();

    	if($gen_doc[0]->user_id != Auth::user()->id){
    		die("Невозможно удалить чужой документ.");
    	}  

    	GenDoc::where('id', $doc_id)->delete();
    	Answer::where('gen_doc_id', $doc_id)->delete();

    	return back()->with('msg', 'Документ успешно удален!');

    }

    function mydocs(Request $r){

    	$all_docs = GenDoc::where('user_id', Auth::user()->id)->get();

    	return view("front.mydocs", ["all_docs"=>$all_docs]);
    }

    function cabinet(){

    	return view("front.cabinet");
    }

    function update_user_info(Request $r){

    	if(strlen($r->input("password")) >= 4){
    		$password = Hash::make($r->input('password'));
    	}else if(strlen($r->input("password")) > 2 && strlen($r->input("password")) < 4){
    		return back()->with('error', "Пароль не может быть менее 4 символов");
    	}else{
    		$data = User::find(Auth::user()->id);
    		$password = $data->password;
    	}

    	User::where('id', Auth::user()->id)->update(["name"=>$r->input('name'), "surname"=>$r->input('surname'), "email"=>$r->input('email'), "phone"=>$r->input('phone'), "password"=>$password]);
    	return back()->with('msg', "Данные успешно обновлены");
    }

}
