<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\GenDoc;
use App\Models\Step12Task;
use App\Models\Step12Link;
use App\Models\Step6;
use Auth;
use Carbon\Carbon;
use DateTimeZone;
use App\Exports\Step12TaskExport;
use Maatwebsite\Excel\Facades\Excel;

class Step12Controller extends Controller
{
    public function stepNew (){

    	$ar['all_docs'] = GenDoc::where('user_id', Auth::user()->id)->where('form_id', 4)->get();

    	return view('front2.steps.step12.step_new', $ar);
    }

    public function exportExcel($doc_id){

        return Excel::download(new Step12TaskExport($doc_id), 'задачи.xlsx');
    }

    public function createNew ($create_from_doc_id, Request $r){

    	$name = $r->input('gen_doc_name');
    	$form_id = 6;
    	$user_id = Auth::user()->id;

    	$gen_doc = new GenDoc();
    	$gen_doc->name = $name;
    	$gen_doc->form_id = $form_id;
    	$gen_doc->user_id = $user_id;
    	$gen_doc->save();

    	$root_task = Step6::where('gen_doc_id', $create_from_doc_id)->where('parent_id', 0)->get();
    	
    	$step12_task = new Step12Task();
		$step12_task->text = $root_task[0]->name;
		$step12_task->duration = 400;
		$step12_task->progress = 0;
		$step12_task->start_date = Carbon::now(new DateTimeZone('Asia/Almaty'));
		$step12_task->parent = 0;
		$step12_task->gen_doc_id = $gen_doc->id;
		$step12_task->save();

    	$this->insertGanttTask($root_task[0]->id, $step12_task->id, $gen_doc->id);

    	return redirect("/project/step/12/doc/".$gen_doc->id)->with("msg", "Документ успешно создан!");

    }
    //Рекурсивная вставка задач из таблицы step6 (иерархия задач) в таблицу step12_tasks (расписание работ)
    public function insertGanttTask ($step6_task_id, $parent_id, $gen_doc_id){
    	$tasks = Step6::where('parent_id', $step6_task_id)->get();
    	foreach ($tasks as $task) {
    		$step12_task = new Step12Task();
    		$step12_task->text = $task->name;
    		$step12_task->duration = 30;
    		$step12_task->progress = 0;
    		$step12_task->start_date = Carbon::now(new DateTimeZone('Asia/Almaty'));
    		$step12_task->parent = $parent_id;
    		$step12_task->gen_doc_id = $gen_doc_id;
    		$step12_task->save();
    		$this->insertGanttTask($task->id, $step12_task->id, $gen_doc_id);
    	}
    }

    public function docEdit ($doc_id){

    	$ar['doc_id'] = $doc_id;
        $ar['doc'] = GenDoc::where('id', $doc_id)->get();
    	return view('front2.steps.step12.step_edit', $ar);
    }

    public function nameUpdate (Request $r, $doc_id){

        GenDoc::where('id', $doc_id)->update(["name" => $r->input("doc_name")]);
        return back()->with('msg', 'Название изменено!');
    }

    public function getGanttJson($doc_id){
		$tasks = new Step12Task();
		$links = new Step12Link();

		return response()->json([
			"data" => $tasks->where("gen_doc_id", $doc_id)->orderBy('sortorder')->get(),
			"links" => $links->where("gen_doc_id", $doc_id)->get()
		]);
	}

	public function docDelete ($doc_id){

		GenDoc::where('id', $doc_id)->delete();
		Step12Task::where('gen_doc_id', $doc_id)->delete();
		Step12Link::where('gen_doc_id', $doc_id)->delete();

		return back()->with('msg', 'Документ успешно удален!');
	}
}
