<?php

namespace App\Http\Middleware;

use Closure;

class setRusLocal
{
    
    public function handle($request, Closure $next)
    {
        
        app()->setlocale('ru');
        session()->put('language', 'ru');
    
        return $next($request);
    }
}
