<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\GenDoc;
use Auth;

class CheckDocAssign
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->route('doc_id') != null){
            $doc_data = GenDoc::where('id', $request->route('doc_id'))->get();
            $doc_count = GenDoc::where('id', $request->route('doc_id'))->count();
            if($doc_count == 0){
                abort(404);
            }
            if($doc_data[0]->user_id != Auth::user()->id){
                abort(403);
            }else{
                return $next($request);
            }
        }else{
            return $next($request);
        }
    }
}
