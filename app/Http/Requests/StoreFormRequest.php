<?php

namespace App\Http\Requests;

use App\Models\Form;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class StoreFormRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('form_create');
    }

    public function rules()
    {
        return [
            'form_name'     => [
                'string',
                'required',
            ],
            'word_template' => [
                'required',
            ],
            'youtube_url'   => [
                'string',
                'nullable',
            ],
        ];
    }
}
