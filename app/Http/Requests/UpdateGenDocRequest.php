<?php

namespace App\Http\Requests;

use App\Models\GenDoc;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class UpdateGenDocRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('gen_doc_edit');
    }

    public function rules()
    {
        return [
            'form_id' => [
                'required',
                'integer',
            ],
            'user_id' => [
                'required',
                'integer',
            ],
            'name'    => [
                'string',
                'nullable',
            ],
        ];
    }
}
