<?php

namespace App\Http\Requests;

use App\Models\Step;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class StoreStepRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('step_create');
    }

    public function rules()
    {
        return [
            'form_id'    => [
                'required',
                'integer',
            ],
            'name'       => [
                'string',
                'required',
            ],
            'form_place' => [
                'nullable',
                'integer',
                'min:-2147483648',
                'max:2147483647',
            ],
        ];
    }
}
