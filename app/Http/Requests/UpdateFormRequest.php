<?php

namespace App\Http\Requests;

use App\Models\Form;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class UpdateFormRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('form_edit');
    }

    public function rules()
    {
        return [
            'form_name'   => [
                'string',
                'required',
            ],
            'youtube_url' => [
                'string',
                'nullable',
            ],
        ];
    }
}
