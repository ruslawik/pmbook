<?php

namespace App\Http\Requests;

use App\Models\GenDoc;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class MassDestroyGenDocRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('gen_doc_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'ids'   => 'required|array',
            'ids.*' => 'exists:gen_docs,id',
        ];
    }
}
