<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Step12Task extends Model
{
    protected $appends = ["open"];

	public function getOpenAttribute(){
		return true;
	}

	public function hasChildren($task_id){
		$children_tasks_count = Step12Task::where('parent', $task_id)->count();

		if($children_tasks_count > 0){
            return 1;
        }else{
            return 0;
        }
	}

	public function children($task_id){
		$children_tasks = Step12Task::where('parent', $task_id)->get();

		return $children_tasks;
	}
}
