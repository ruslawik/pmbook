<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Step14 extends Model
{
    protected $table = "step14";

    public function hasChildren($block_id){

    	$count = $this->where('parent_id', $block_id)->count();
    	if($count > 0){
    		return 1;
    	}else{
    		return 0;
    	}
    }

    public function children($block_id){
    	
    	$statiyas = $this->where('parent_id', $block_id)->get();
    	return $statiyas;
    }

}
