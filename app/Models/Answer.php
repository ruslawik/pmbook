<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use \DateTimeInterface;

class Answer extends Model
{
    use SoftDeletes;

    public $table = 'answers';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'answer',
        'field_id',
        'gen_doc_id',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    public function field()
    {
        return $this->belongsTo(Field::class, 'field_id');
    }

    public function gen_doc()
    {
        return $this->belongsTo(GenDoc::class, 'gen_doc_id');
    }
}
