<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Answer;
use \DateTimeInterface;
use Spatie\Translatable\HasTranslations;

class Field extends Model
{
    use SoftDeletes, HasTranslations;

    public $table = 'fields';

    public $translatable = ['user_name'];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'name',
        'user_name',
        'user_name_kz',
        'user_name_en',
        'field_type',
        'form_id',
        'step_id',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    const FIELD_TYPE_SELECT = [
        'text'        => 'text',
        'textarea'    => 'textarea',
        'textarea_ck' => 'textarea ckeditor',
        'checkbox'    => 'checkbox',
        'select'      => 'select',
        'radio'       => 'radio',
    ];

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    public function form()
    {
        return $this->belongsTo(Form::class, 'form_id');
    }

    public function step()
    {
        return $this->belongsTo(Step::class, 'step_id');
    }

    public function field_value($field_id, $gen_doc_id){

        $answer = Answer::where('field_id', $field_id)->where('gen_doc_id', $gen_doc_id)->get();

        return $answer[0]->answer;
    }

    public function field_id($field_id, $gen_doc_id){

        $answer = Answer::where('field_id', $field_id)->where('gen_doc_id', $gen_doc_id)->get();

        return $answer[0]->id;
    }
}
