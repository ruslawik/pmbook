<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;
use \DateTimeInterface;
use Spatie\Translatable\HasTranslations;

class Page extends Model implements HasMedia
{
    use SoftDeletes, HasMediaTrait, HasTranslations;
    
    public $translatable = ['name', 'content'];

    public $table = 'pages';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'name',
        'name_en',
        'name_kz',
        'content',
        'content_en',
        'content_kz',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')->fit('crop', 50, 50)->nonOptimized()->nonQueued();
        $this->addMediaConversion('preview')->fit('crop', 120, 120)->nonOptimized()->nonQueued();
    }
}
