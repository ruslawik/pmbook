<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;
use \DateTimeInterface;
use Spatie\Translatable\HasTranslations;

class Form extends Model implements HasMedia
{
    use SoftDeletes, HasMediaTrait, HasTranslations;

    public $translatable = ['form_name'];

    public $table = 'forms';

    protected $appends = [
        'word_template',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'form_name',
        'form_name_kz',
        'form_name_en',
        'description',
        'youtube_url',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')->fit('crop', 50, 50)->nonOptimized()->nonQueued();
        $this->addMediaConversion('preview')->fit('crop', 120, 120)->nonOptimized()->nonQueued();
    }

    public function formFields()
    {
        return $this->hasMany(Field::class, 'form_id', 'id');
    }

    public function formSteps()
    {
        return $this->hasMany(Step::class, 'form_id', 'id')->orderBy('form_place', 'ASC');
    }

    public function getWordTemplateAttribute()
    {
        return $this->getMedia('word_template')->last();
    }
}
