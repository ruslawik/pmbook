<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;
use \DateTimeInterface;
use Spatie\Translatable\HasTranslations;

class Step extends Model implements HasMedia
{
    use SoftDeletes, HasMediaTrait, HasTranslations;

    public $table = 'steps';

    public $translatable = ['name'];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'form_id',
        'name',
        'name_kz',
        'name_en',
        'description',
        'form_place',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')->fit('crop', 50, 50)->nonOptimized()->nonQueued();
        $this->addMediaConversion('preview')->fit('crop', 120, 120)->nonOptimized()->nonQueued();
    }

    public function stepFields()
    {
        return $this->hasMany(Field::class, 'step_id', 'id');
    }

    public function form()
    {
        return $this->belongsTo(Form::class, 'form_id');
    }
}
