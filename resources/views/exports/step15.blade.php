<table style="width:100% !important;">
    <thead id="head_fixed">
    <tr>
        <th><b>№</b></th>
        <th width=60 style="font-size:16px;"><b>НАИМЕНОВАНИЕ РИСКА</b></th>
        <th width=39 style="font-size:16px;"><b>ВЕРОЯТНОСТЬ НАСТУПЛЕНИЯ</b></th>
        <th width=25 style="font-size:16px;"><b>УРОВЕНЬ ВЛИЯНИЯ</b></th>
        <th width=20 style="font-size:16px;"><b>СТРАТЕГИЯ</b></th>
        <th width=35 style="font-size:16px;"><b>ТАКТИКА</b></th>
    </tr>
    </thead>
    <tbody>
        <?php 
            $i = 1;
        ?>
        @foreach($risks as $risk)
            <tr onClick="edit_risk({{$risk->id}});">
                <td style="font-size:14px;" rowspan=2>{{$i}}</td>
                <td style="font-size:14px;" rowspan=2>{{$risk->name}}</td>
                <td style="font-size:14px;">{{$risk->ver}}</td>
                <td style="font-size:14px;">{{$risk->ur}}</td>
                <td style="font-size:14px;" rowspan=2>{{$strats[$risk->strat]}}</td>
                <td style="font-size:14px;" rowspan=2>{{$risk->takt}}</td>
            </tr>
            <tr onClick="edit_risk({{$risk->id}});">
                <td style="font-size:14px;" colspan="2">
                    {{$risk->ver*$risk->ur}}
                </td>
            </tr>
            <?php
                $i++;
            ?>
        @endforeach
    </tbody>
</table>