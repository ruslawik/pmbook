<table style="width:100% !important;">
    <thead>
    <tr>
        <th width=30><b>ЗАДАЧА</b></th>
        <th width=30><b>ПОКАЗАТЕЛЬ</b></th>
        <th width=10><b>ЕД. ИЗМ.</b></th>
        <th width=30><b>КРИТЕРИИ</b></th>
        <th width=30><b>ЦЕЛЕВ. ПОКАЗ.</b></th>
        <th width=10><b>ЕД. ИЗМ.</b></th>
    </tr>
    </thead>
    <tbody>
        @foreach($tasks as $task)
            <tr>
                <td>{{$task->task_text}}</td>
                <td>{{$task->pokaz}}</td>
                <td>{{$task->pokaz_ed_izm}}</td>
                <td>{{$task->criteria}}</td>
                <td>{{$task->goal_pokaz}}</td>
                <td>{{$task->goal_pokaz_ed_izm}}</td>
            </tr>
        @endforeach
    </tbody>
</table>