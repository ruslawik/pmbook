<table>
    <thead>
    <tr>
        <th style="font-size:14px !important;"><b>№</b></th>
        <th style="font-size:14px !important;" width=80><b>НАИМЕНОВАНИЕ СТАТЕЙ ЗАТРАТ</b></th>
        <th style="font-size:14px !important;" width=30><b>СТОИМОСТЬ ЗА ЕДИНИЦУ</b></th>
        <th style="font-size:14px !important;" width=30><b>ВРЕМЯ РЕСУРСА В ПРОЕКТЕ</b></th>
        <th style="font-size:14px !important;" width=30><b>СУММА</b></th>
    </tr>
    </thead>
    <tbody>
        <?php
            $i = 1;
        ?>
        @foreach($blocks as $block)
            <tr>
                <th colspan="2" style="font-size:16 !important; text-align: center !important; background-color: #F4DD77;">
                </th>
                <th colspan="3" style="font-size:16 !important; text-align: center !important; background-color: #F4DD77;">
                    {{$block->statiya_name}}
                </th>
            </tr>
            @if($block->hasChildren($block->id) == 1)
                @foreach($block->children($block->id) as $statiya)
                    <tr>
                        <td style="font-size:14px !important;">{{$i}}</td>
                        <td style="font-size:14px !important;">{{$statiya->statiya_name}}</td>
                        <td style="font-size:14px !important;">{{$statiya->price_for_one}}</td>
                        <td style="font-size:14px !important;">{{$statiya->time_in_project}}</td>
                        <td style="font-size:14px !important;">{{$statiya->total_sum}}</td>
                    </tr>
                    <?php
                        $i++;
                    ?>
                @endforeach
            @endif
        @endforeach
    </tbody>
</table>