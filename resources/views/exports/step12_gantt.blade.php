<table>
    
    <thead>
        <tr>
            <th width="100" style="font-size:14 !important;">
                <b>Задача</b>
            </th>
            <th width="30" style="font-size:14 !important;">
                <b>Дата начала</b>
            </th>
            <th width="10" style="font-size:14 !important;">
                <b>Продолжительность</b>
            </th>
            <?php
                $current_month = $min_datetime;
                $i=0;
                while($current_month < $max_datetime){
                    echo "<th width='12' style='font-size:14 !important;'>";
                    echo "<b>".$current_month->format('M Y')."</b>";
                    echo "</th>";
                    $current_month->addDays(31);
                    $i++;
                }
                $current_month->subDays(31*$i);
            ?>
        </tr>
    </thead>

    <tbody>
        @foreach($tasks as $task)
            <tr>
                <td style="font-size:14 !important; background-color: #F4DD77;">{{$task->text}}</td>
                <td style="font-size:14 !important;">{{$task->start_date}}</td>
                <td style="font-size:14 !important;">{{$task->duration}}</td>
                <?php
                    while($current_month < $max_datetime){
                        $task_end_date = new \Carbon\Carbon($task->start_date);
                        $task_end_date->addDays($task->duration);
                        $task_start_date = new \Carbon\Carbon($task->start_date);
                        if($task_start_date <= $current_month && $task_end_date >= $current_month){
                            echo "<td style='background-color: #4574CB;'>";
                        }else{
                            echo "<td>";
                        }
                        $current_month->addDays(31);
                    }
                    $current_month->subDays(31*$i);
                ?>
            </tr>
            @if($task->hasChildren($task->id) == 1)
                @include('exports.step12_child_tr', ["tasks" => $task->children($task->id), "min_datetime" => $current_month, "max_datetime" => $max_datetime, "level" => 1])
            @endif
        @endforeach
    </tbody>

</table>