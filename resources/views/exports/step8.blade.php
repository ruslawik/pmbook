<link href="/assets/8_step_style.css" rel="stylesheet" type="text/css">
<div class="table__content" id="page-content">
    <table>
        <thead id="head_fixed">
        <tr>
            <th rowspan="2" style="border:1px solid silver; background-color: #DCEEF0;"><b>№</b></th>
            <th width="30" style="border:1px solid silver; background-color: #DCEEF0; font-size:14 !important;" rowspan="2"><b>Наименование ресурса</b></th>
            <th width="20" style="border:1px solid silver; background-color: #DCEEF0; font-size:14 !important;" rowspan="2"><b>Тип ресурса</b></th>
            <th width="70" style="border:1px solid silver; background-color: #DCEEF0; font-size:14 !important;" rowspan="2"><b>Дата и номер конракта на приобретение/привлечение ресурса</b></th>
            <th width="50" style="border:1px solid silver; background-color: #DCEEF0; font-size:14 !important;" rowspan="2"><b>Дата высвобождения ресурса с проекта</b></th>
            <th width="35" style="border:1px solid silver; background-color: #DCEEF0; font-size:14 !important;" rowspan="2"><b>Логистический путь ресурса</b></th>
            <th width="100" style="border:1px solid silver; background-color: #DCEEF0; font-size:14 !important;" colspan="4"><b>Процент загрузки на период этапов проекта</b></th>
            <th width="30" style="border:1px solid silver; background-color: #DCEEF0; font-size:14 !important;" rowspan="2"><b>Объем/Количество</b></th>
        </tr>
        <tr>
            <th width="25" style="border:1px solid silver; background-color: #DCEEF0; font-size:14 !important;">Инициирование</th>
            <th width="25" style="border:1px solid silver; background-color: #DCEEF0; font-size:14 !important;">Планирование</th>
            <th width="25" style="border:1px solid silver; background-color: #DCEEF0; font-size:14 !important;">Реализация</th>
            <th width="25" style="border:1px solid silver; background-color: #DCEEF0; font-size:14 !important;">Контроль и завершение</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <th colspan="4" style="font-size:14 !important; text-align: center !important; background-color: #F4DD77; "></th>
            <th colspan="7" style="font-size:14 !important; text-align: center !important; background-color: #F4DD77;">Человеческие ресурсы</th>
        </tr>
        <tr>
            <td>1</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>2</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>3</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr id="row1">
            <th colspan="4" style="font-size:14 !important; text-align: center !important; background-color: #F4DD77; "></th>
            <th colspan="7" style="font-size:14 !important; text-align: center !important; background-color: #F4DD77; ">Оборудование</th>
        </tr>
        <tr>
            <td>1</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>2</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>3</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr id="row2">
            <th colspan="4" style="font-size:14 !important; text-align: center !important; background-color: #F4DD77; "></th>
            <th colspan="7" style="font-size:14 !important; text-align: center !important; background-color: #F4DD77; ">Сырье/Материалы</th>
        </tr>
        <tr>
            <td>1</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>2</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>3</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr id="row3">
            <th colspan="4" style="font-size:14 !important; text-align: center !important; background-color: #F4DD77; "></th>
            <th colspan="7" style="font-size:14 !important; text-align: center !important; background-color: #F4DD77; ">Информация и данные</th>
        </tr>
        <tr>
            <td>1</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>2</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>3</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr id="row4">
            <th colspan="4" style="font-size:14 !important; text-align: center !important; background-color: #F4DD77; "></th>
            <th colspan="7" style="font-size:14 !important; text-align: center !important; background-color: #F4DD77; ">Услуги</th>
        </tr>
        <tr>
            <td>1</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>2</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>3</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr id="row5">
            <th colspan="4" style="font-size:14 !important; text-align: center !important; background-color: #F4DD77; "></th>
            <th colspan="7" style="font-size:14 !important; text-align: center !important; background-color: #F4DD77; ">Расходные материалы</th>
        </tr>
        <tr>
            <td>1</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>2</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>3</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr id="row6">
            <th colspan="4" style="font-size:14 !important; text-align: center !important; background-color: #F4DD77; "></th>
            <th colspan="7" style="font-size:14 !important; text-align: center !important; background-color: #F4DD77; ">Информационные и коммуникационные технологии (ИКТ)</th>
        </tr>
        <tr>
            <td>1</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>2</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>3</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr id="row7">
            <th colspan="4" style="font-size:14 !important; text-align: center !important; background-color: #F4DD77; "></th>
            <th colspan="7" style="font-size:14 !important; text-align: center !important; background-color: #F4DD77; ">Транспорт и логистика</th>
        </tr>
        <tr>
            <td>1</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>2</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>3</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr id="row8">
            <th colspan="4" style="font-size:14 !important; text-align: center !important; background-color: #F4DD77; "></th>
            <th colspan="7" style="font-size:14 !important; text-align: center !important; background-color: #F4DD77; ">ЗСП</th>
        </tr>
        <tr>
            <td>1</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>2</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>3</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr id="row9">
            <th colspan="4" style="font-size:14 !important; text-align: center !important; background-color: #F4DD77; "></th>
            <th colspan="7" style="font-size:14 !important; text-align: center !important; background-color: #F4DD77; ">Финансы</th>
        </tr>
        <tr>
            <td>1</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>2</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>3</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr id="row10">
        </tr>
        </tbody>
    </table>
</div>                                 