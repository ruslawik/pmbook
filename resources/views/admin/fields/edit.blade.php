@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.edit') }} {{ trans('cruds.field.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.fields.update", [$field->id]) }}" enctype="multipart/form-data">
            @method('PUT')
            @csrf
            <div class="form-group">
                <label class="required" for="name">{{ trans('cruds.field.fields.name') }}</label>
                <input class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" type="text" name="name" id="name" value="{{ old('name', $field->name) }}" required>
                @if($errors->has('name'))
                    <span class="text-danger">{{ $errors->first('name') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.field.fields.name_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="user_name">{{ trans('cruds.field.fields.user_name') }}</label>
                <input class="form-control {{ $errors->has('user_name') ? 'is-invalid' : '' }}" type="text" name="user_name" id="user_name" value="{{ old('user_name', $field->user_name) }}" required>
                @if($errors->has('user_name'))
                    <span class="text-danger">{{ $errors->first('user_name') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.field.fields.user_name_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="user_name_kz">{{ trans('cruds.field.fields.user_name_kz') }}</label>
                <input class="form-control {{ $errors->has('user_name_kz') ? 'is-invalid' : '' }}" type="text" name="user_name_kz" id="user_name_kz" value="{{ old('user_name_kz', $field->user_name_kz) }}">
                @if($errors->has('user_name_kz'))
                    <span class="text-danger">{{ $errors->first('user_name_kz') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.field.fields.user_name_kz_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="user_name_kz">Наименование для пользователя на английском</label>
                <input class="form-control {{ $errors->has('user_name_en') ? 'is-invalid' : '' }}" type="text" name="user_name_en" id="user_name_en" value="{{ old('user_name_en', $field->user_name_en) }}">
                @if($errors->has('user_name_en'))
                    <span class="text-danger">{{ $errors->first('user_name_en') }}</span>
                @endif
            </div>
            <div class="form-group">
                <label class="required">{{ trans('cruds.field.fields.field_type') }}</label>
                <select class="form-control {{ $errors->has('field_type') ? 'is-invalid' : '' }}" name="field_type" id="field_type" required>
                    <option value disabled {{ old('field_type', null) === null ? 'selected' : '' }}>{{ trans('global.pleaseSelect') }}</option>
                    @foreach(App\Models\Field::FIELD_TYPE_SELECT as $key => $label)
                        <option value="{{ $key }}" {{ old('field_type', $field->field_type) === (string) $key ? 'selected' : '' }}>{{ $label }}</option>
                    @endforeach
                </select>
                @if($errors->has('field_type'))
                    <span class="text-danger">{{ $errors->first('field_type') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.field.fields.field_type_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="form_id">{{ trans('cruds.field.fields.form') }}</label>
                <select class="form-control select2 {{ $errors->has('form') ? 'is-invalid' : '' }}" name="form_id" id="form_id">
                    @foreach($forms as $id => $form)
                        <option value="{{ $id }}" {{ (old('form_id') ? old('form_id') : $field->form->id ?? '') == $id ? 'selected' : '' }}>{{ $form }}</option>
                    @endforeach
                </select>
                @if($errors->has('form'))
                    <span class="text-danger">{{ $errors->first('form') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.field.fields.form_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="step_id">{{ trans('cruds.field.fields.step') }}</label>
                <select class="form-control select2 {{ $errors->has('step') ? 'is-invalid' : '' }}" name="step_id" id="step_id">
                    @foreach($steps as $id => $step)
                        <option value="{{ $id }}" {{ (old('step_id') ? old('step_id') : $field->step->id ?? '') == $id ? 'selected' : '' }}>{{ $step }}</option>
                    @endforeach
                </select>
                @if($errors->has('step'))
                    <span class="text-danger">{{ $errors->first('step') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.field.fields.step_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection