@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.field.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.fields.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.field.fields.id') }}
                        </th>
                        <td>
                            {{ $field->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.field.fields.name') }}
                        </th>
                        <td>
                            {{ $field->name }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.field.fields.user_name') }}
                        </th>
                        <td>
                            {{ $field->user_name }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.field.fields.user_name_kz') }}
                        </th>
                        <td>
                            {{ $field->user_name_kz }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.field.fields.field_type') }}
                        </th>
                        <td>
                            {{ App\Models\Field::FIELD_TYPE_SELECT[$field->field_type] ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.field.fields.form') }}
                        </th>
                        <td>
                            {{ $field->form->form_name ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.field.fields.step') }}
                        </th>
                        <td>
                            {{ $field->step->name ?? '' }}
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.fields.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>



@endsection