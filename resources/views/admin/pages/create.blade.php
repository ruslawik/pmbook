@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('cruds.page.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.pages.store") }}" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label class="required" for="name">{{ trans('cruds.page.fields.name') }}</label>
                <input class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" type="text" name="name" id="name" value="{{ old('name', '') }}" required>
                @if($errors->has('name'))
                    <span class="text-danger">{{ $errors->first('name') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.page.fields.name_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="name_en">{{ trans('cruds.page.fields.name_en') }}</label>
                <input class="form-control {{ $errors->has('name_en') ? 'is-invalid' : '' }}" type="text" name="name_en" id="name_en" value="{{ old('name_en', '') }}">
                @if($errors->has('name_en'))
                    <span class="text-danger">{{ $errors->first('name_en') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.page.fields.name_en_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="name_kz">{{ trans('cruds.page.fields.name_kz') }}</label>
                <input class="form-control {{ $errors->has('name_kz') ? 'is-invalid' : '' }}" type="text" name="name_kz" id="name_kz" value="{{ old('name_kz', '') }}">
                @if($errors->has('name_kz'))
                    <span class="text-danger">{{ $errors->first('name_kz') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.page.fields.name_kz_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="content">{{ trans('cruds.page.fields.content') }}</label>
                <textarea class="form-control ckeditor {{ $errors->has('content') ? 'is-invalid' : '' }}" name="content" id="content">{!! old('content') !!}</textarea>
                @if($errors->has('content'))
                    <span class="text-danger">{{ $errors->first('content') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.page.fields.content_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="content_en">{{ trans('cruds.page.fields.content_en') }}</label>
                <textarea class="form-control ckeditor {{ $errors->has('content_en') ? 'is-invalid' : '' }}" name="content_en" id="content_en">{!! old('content_en') !!}</textarea>
                @if($errors->has('content_en'))
                    <span class="text-danger">{{ $errors->first('content_en') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.page.fields.content_en_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="content_kz">{{ trans('cruds.page.fields.content_kz') }}</label>
                <textarea class="form-control ckeditor {{ $errors->has('content_kz') ? 'is-invalid' : '' }}" name="content_kz" id="content_kz">{!! old('content_kz') !!}</textarea>
                @if($errors->has('content_kz'))
                    <span class="text-danger">{{ $errors->first('content_kz') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.page.fields.content_kz_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection

@section('scripts')
<script>
    $(document).ready(function () {
  function SimpleUploadAdapter(editor) {
    editor.plugins.get('FileRepository').createUploadAdapter = function(loader) {
      return {
        upload: function() {
          return loader.file
            .then(function (file) {
              return new Promise(function(resolve, reject) {
                // Init request
                var xhr = new XMLHttpRequest();
                xhr.open('POST', '/admin/pages/ckmedia', true);
                xhr.setRequestHeader('x-csrf-token', window._token);
                xhr.setRequestHeader('Accept', 'application/json');
                xhr.responseType = 'json';

                // Init listeners
                var genericErrorText = `Couldn't upload file: ${ file.name }.`;
                xhr.addEventListener('error', function() { reject(genericErrorText) });
                xhr.addEventListener('abort', function() { reject() });
                xhr.addEventListener('load', function() {
                  var response = xhr.response;

                  if (!response || xhr.status !== 201) {
                    return reject(response && response.message ? `${genericErrorText}\n${xhr.status} ${response.message}` : `${genericErrorText}\n ${xhr.status} ${xhr.statusText}`);
                  }

                  $('form').append('<input type="hidden" name="ck-media[]" value="' + response.id + '">');

                  resolve({ default: response.url });
                });

                if (xhr.upload) {
                  xhr.upload.addEventListener('progress', function(e) {
                    if (e.lengthComputable) {
                      loader.uploadTotal = e.total;
                      loader.uploaded = e.loaded;
                    }
                  });
                }

                // Send request
                var data = new FormData();
                data.append('upload', file);
                data.append('crud_id', '{{ $page->id ?? 0 }}');
                xhr.send(data);
              });
            })
        }
      };
    }
  }

  var allEditors = document.querySelectorAll('.ckeditor');
  for (var i = 0; i < allEditors.length; ++i) {
    ClassicEditor.create(
      allEditors[i], {
        extraPlugins: [SimpleUploadAdapter]
      }
    );
  }
});
</script>

@endsection