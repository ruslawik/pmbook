@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.form.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.forms.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.form.fields.id') }}
                        </th>
                        <td>
                            {{ $form->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.form.fields.form_name') }}
                        </th>
                        <td>
                            {{ $form->form_name }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.form.fields.word_template') }}
                        </th>
                        <td>
                            @if($form->word_template)
                                <a href="{{ $form->word_template->getUrl() }}" target="_blank">
                                    {{ trans('global.view_file') }}
                                </a>
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.form.fields.description') }}
                        </th>
                        <td>
                            {!! $form->description !!}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.form.fields.youtube_url') }}
                        </th>
                        <td>
                            {{ $form->youtube_url }}
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.forms.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>

<div class="card">
    <div class="card-header">
        {{ trans('global.relatedData') }}
    </div>
    <ul class="nav nav-tabs" role="tablist" id="relationship-tabs">
        <li class="nav-item">
            <a class="nav-link" href="#form_fields" role="tab" data-toggle="tab">
                {{ trans('cruds.field.title') }}
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#form_steps" role="tab" data-toggle="tab">
                {{ trans('cruds.step.title') }}
            </a>
        </li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane" role="tabpanel" id="form_fields">
            @includeIf('admin.forms.relationships.formFields', ['fields' => $form->formFields])
        </div>
        <div class="tab-pane" role="tabpanel" id="form_steps">
            @includeIf('admin.forms.relationships.formSteps', ['steps' => $form->formSteps])
        </div>
    </div>
</div>

@endsection