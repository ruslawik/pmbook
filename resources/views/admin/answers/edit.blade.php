@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.edit') }} {{ trans('cruds.answer.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.answers.update", [$answer->id]) }}" enctype="multipart/form-data">
            @method('PUT')
            @csrf
            <div class="form-group">
                <label class="required" for="field_id">{{ trans('cruds.answer.fields.field') }}</label>
                <select class="form-control select2 {{ $errors->has('field') ? 'is-invalid' : '' }}" name="field_id" id="field_id" required>
                    @foreach($fields as $id => $field)
                        <option value="{{ $id }}" {{ (old('field_id') ? old('field_id') : $answer->field->id ?? '') == $id ? 'selected' : '' }}>{{ $field }}</option>
                    @endforeach
                </select>
                @if($errors->has('field'))
                    <span class="text-danger">{{ $errors->first('field') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.answer.fields.field_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="gen_doc_id">{{ trans('cruds.answer.fields.gen_doc') }}</label>
                <select class="form-control select2 {{ $errors->has('gen_doc') ? 'is-invalid' : '' }}" name="gen_doc_id" id="gen_doc_id" required>
                    @foreach($gen_docs as $id => $gen_doc)
                        <option value="{{ $id }}" {{ (old('gen_doc_id') ? old('gen_doc_id') : $answer->gen_doc->id ?? '') == $id ? 'selected' : '' }}>{{ $gen_doc }}</option>
                    @endforeach
                </select>
                @if($errors->has('gen_doc'))
                    <span class="text-danger">{{ $errors->first('gen_doc') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.answer.fields.gen_doc_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection