@extends('front2.layouts.main_layout')
@section('content')
    
<div class="page-content-wrapper ">

                        <div class="container-fluid">

                            <div class="row">
                                <div class="col-sm-3">
                                </div>
                                    <div class="col-sm-6">
                                        <div class="page-title-box">
                                            <br>
    <div class="card col-12">
                <div class="card-body">
                    <br>
                    <h4 class="text-center mt-0 m-b-15">
                        <a href="/login">Вход</a>/<a href="/register">Регистрация</a>
                    </h4>

                    <div class="p-3">
                        @if(session()->has('message'))
                            <p class="alert alert-info">
                                {{ session()->get('message') }}
                            </p>
                        @endif
                        <form action="{{ route('register') }}" method="POST" class="form-horizontal">
                                    {{csrf_field()}}

                                    <div class="form-group">
                                        <label for="name">Имя</label>
                                        <input type="text" id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" required autocomplete="name" autofocus placeholder="{{ trans('global.name') }}" name="name" value="{{ old('name', null) }}" placeholder="Введите имя">

                                        @if($errors->has('name'))
                                            <div class="invalid-feedback">
                                                {{ $errors->first('name') }}
                                            </div>
                                        @endif
                                    </div>


                                    <div class="form-group">
                                        <label for="email">Email</label>
                                        <input type="text" id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" required autocomplete="email" autofocus placeholder="{{ trans('global.login_email') }}" name="email" value="{{ old('email', null) }}" placeholder="Введите email">

                                        @if($errors->has('email'))
                                            <div class="invalid-feedback">
                                                {{ $errors->first('email') }}
                                            </div>
                                        @endif
                                    </div>

                                    <div class="form-group">
                                        <label for="password">Пароль</label>
                                        <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required placeholder="{{ trans('global.login_password') }}">
                                        @if($errors->has('password'))
                                            <div class="invalid-feedback">
                                                {{ $errors->first('password') }}
                                            </div>
                                        @endif
                                    </div>

                                    <div class="form-group">
                                        <label for="password-confirm">Подтвердите пароль</label>
                                        <input id="password-confirm" type="password" class="form-control{{ $errors->has('password-confirm') ? ' is-invalid' : '' }}" name="password_confirmation" required autocomplete="new-password" placeholder="Подтвердите пароль">
                                        @if($errors->has('password-confirm'))
                                            <div class="invalid-feedback">
                                                {{ $errors->first('password-confirm') }}
                                            </div>
                                        @endif
                                    </div>

                                    <div class="mt-3">
                                        <button class="btn btn-danger btn-block waves-effect waves-light" type="submit">Войти</button>
                                    </div>
                                </form>
                    </div>

                </div>
            </div></div></div></div></div>

@endsection




