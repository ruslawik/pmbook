@extends('front2.layouts.main_layout')
@section('content')
    
<div class="page-content-wrapper ">

                        <div class="container-fluid">

                            <div class="row">
                                <div class="col-sm-3">
                                </div>
                                    <div class="col-sm-6">
                                        <div class="page-title-box">
                                            <br><br><br><br>
    <div class="card col-12">
                <div class="card-body">
                    <br>
                    <h4 class="text-center mt-0 m-b-15">
                        <a href="/login">Вход</a>/<a href="/register">Регистрация</a>
                    </h4>

                    <div class="p-3">
                        @if(session()->has('message'))
                            <p class="alert alert-info">
                                {{ session()->get('message') }}
                            </p>
                        @endif
                        <form class="form-horizontal m-t-20" action="{{ route('login') }}" method="POST">
                            @csrf
                            <div class="form-group row">
                                <div class="col-12">
                                    <input class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" type="text" required="required" placeholder="Email" name="email" value="{{ old('email', null) }}">
                                </div>
                                @if($errors->has('email'))
                                            <div class="invalid-feedback">
                                                {{ $errors->first('email') }}
                                            </div>
                                        @endif
                            </div>

                            <div class="form-group row">
                                <div class="col-12">
                                    <input class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" type="password" required="" placeholder="Пароль" name="password">
                                </div>
                                @if($errors->has('password'))
                                            <div class="invalid-feedback">
                                                {{ $errors->first('password') }}
                                            </div>
                                        @endif
                            </div>


                            <div class="form-group text-center row m-t-20">
                                <div class="col-12">
                                    <button class="btn btn-danger btn-block waves-effect waves-light" type="submit">Войти</button>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </div></div></div></div></div>

@endsection



