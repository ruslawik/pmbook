@extends('front2.layouts.main_layout')
@section('content')

	            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="main-content">

                <div class="page-content">

                    <!-- start page title -->
                    <div class="row">
                        <div class="col-12">
                            <div class="page-title-box d-flex align-items-center justify-content-between">
                                <h4 class="page-title mb-0 font-size-18"><br></h4>

                            </div>
                        </div>
                    </div>
                    <!-- end page title -->

                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-body">

                                    <h4 class="card-title">Список Ваших документов</h4>
                                    <p class="card-title-desc">На данной странице Вы можете найти все документы, которые когда-либо были разработаны и скачаны с системы. Все ответы сохранены в базе и Вы можете изменить их и скачать документ снова.
                                    </p>

                                    @if (Session::has('msg'))
                                    <div class="alert alert-success">
                                        {!! Session::get('msg') !!}
                                    </div>
                                    @endif
                                    @if (Session::has('error'))
                                    <div class="alert alert-danger">
                                        <ul>
                                            <li>{{ Session::get('error') }}</li>
                                        </ul>
                                    </div>
                                    @endif

                                    <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                        <thead>
                                            <tr>
                                                <th>ID документа</th>
                                                <th>Вид документа</th>
                                                <th>Название</th>
                                                <th>Дата разработки</th>
                                                <th>Удалить</th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            @foreach($all_docs as $doc)
                                            <tr>
                                                <td>{{$doc->id}}</td>
                                                <td>
                                                    <!--Если это автоматические для заполнения формы!-->
                                                    @if($doc->form_id==1 || $doc->form_id==3 || $doc->form_id==5)
                                                        <a href="/project/doc/{{$doc->id}}/edit">{{$doc->form->form_name}}</a>
                                                    @endif
                                                    <!--Если это иерархическая стуктура работ!-->
                                                    @if($doc->form_id==4)
                                                        <a href="/project/step/6/doc/{{$doc->id}}">{{$doc->form->form_name}}</a>
                                                    @endif
                                                    <!-- Если это расписание проекта!-->
                                                    @if($doc->form_id==6)
                                                        <a href="/project/step/12/doc/{{$doc->id}}">{{$doc->form->form_name}}</a>
                                                    @endif
                                                    <!-- Если это оценка ресурсов проекта!-->
                                                    @if($doc->form_id==7)
                                                        <a href="/project/step/8/edit/{{$doc->id}}">{{$doc->form->form_name}}</a>
                                                    @endif
                                                    <!-- Если это разработка бюджета проекта!-->
                                                    @if($doc->form_id==8)
                                                        <a href="/project/step/14/edit/{{$doc->id}}">{{$doc->form->form_name}}</a>
                                                    @endif
                                                    <!-- Если это определение рисков!-->
                                                    @if($doc->form_id==9)
                                                        <a href="/project/step/15/edit/{{$doc->id}}">{{$doc->form->form_name}}</a>
                                                    @endif
                                                    <!-- Если это план качества проекта!-->
                                                    @if($doc->form_id==10)
                                                        <a href="/project/step/16/doc/{{$doc->id}}">{{$doc->form->form_name}}</a>
                                                    @endif
                                                </td>
                                                <td>{{$doc->name}}</td>
                                                <td>{{$doc->created_at}}<br>
                                                    @if($doc->form_id==1 || $doc->form_id==3 || $doc->form_id==5)
                                                        <a href="/mydoc/{{$doc->id}}/download"><button class="btn btn-primary">Скачать</button></a>
                                                    @endif
                                                    @if($doc->form_id==4)
                                                        <a href="/download/step-6/{{$doc->id}}"><button class="btn btn-primary">Скачать</button></a>
                                                    @endif
                                                    @if($doc->form_id==6)
                                                        <a href="/project/step/12/download-excel/{{$doc->id}}"><button class="btn btn-primary">Скачать</button></a>
                                                    @endif
                                                    @if($doc->form_id==7)
                                                        <a href="/project/step/8/download/{{$doc->id}}"><button class="btn btn-primary">Скачать</button></a>
                                                    @endif
                                                    @if($doc->form_id==8)
                                                        <a href="/project/step/14/download/{{$doc->id}}"><button class="btn btn-primary">Скачать</button></a>
                                                    @endif
                                                    @if($doc->form_id==9)
                                                        <a href="/project/step/15/download/{{$doc->id}}"><button class="btn btn-primary">Скачать</button></a>
                                                    @endif
                                                    @if($doc->form_id==10)
                                                        <a href="/project/step/16/download/{{$doc->id}}"><button class="btn btn-primary">Скачать</button></a>
                                                    @endif
                                                </td>
                                                <td><br>
                                                    @if($doc->form_id==1 || $doc->form_id==3 || $doc->form_id==5)
                                                        <a href="/mydoc/{{$doc->id}}/delete"><button class="btn btn-danger">Удалить</button></a>
                                                    @endif
                                                    @if($doc->form_id==4)
                                                        <a href="/project/step-6/{{$doc->id}}/delete"><button class="btn btn-danger">Удалить</button></a>
                                                    @endif
                                                    @if($doc->form_id==6)
                                                        <a href="/project/step/12/delete/{{$doc->id}}"><button class="btn btn-danger">Удалить</button></a>
                                                    @endif
                                                    @if($doc->form_id==7)
                                                        <a href="/project/step/8/delete/{{$doc->id}}"><button class="btn btn-danger">Удалить</button></a>
                                                    @endif
                                                    @if($doc->form_id==8)
                                                        <a href="/project/step/14/delete/{{$doc->id}}"><button class="btn btn-danger">Удалить</button></a>
                                                    @endif
                                                    @if($doc->form_id==9)
                                                        <a href="/project/step/15/delete/{{$doc->id}}"><button class="btn btn-danger">Удалить</button></a>
                                                    @endif
                                                    @if($doc->form_id==10)
                                                        <a href="/project/step/16/delete/{{$doc->id}}"><button class="btn btn-danger">Удалить</button></a>
                                                    @endif
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!-- end col -->
                    </div>
                    <!-- end row -->

                    <!-- end row -->

                </div>
                <!-- End Page-content -->

@endsection

@section('js_scripts')
<!-- Required datatable js -->
    <script src="/assets/libs/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="/assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
    <!-- Buttons examples -->
    <script src="/assets/libs/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="/assets/libs/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js"></script>
    <script src="/assets/libs/jszip/jszip.min.js"></script>
    <script src="/assets/libs/pdfmake/build/pdfmake.min.js"></script>
    <script src="/assets/libs/pdfmake/build/vfs_fonts.js"></script>
    <script src="/assets/libs/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="/assets/libs/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="/assets/libs/datatables.net-buttons/js/buttons.colVis.min.js"></script>
    <!-- Responsive examples -->
    <script src="/assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="/assets/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js"></script>

    <!-- Datatable init js -->
    <script src="/assets/js/pages/datatables.init.js"></script>

@endsection