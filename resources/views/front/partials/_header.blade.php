<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title>PMBook</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="PMBook by Zarina Biyumbayeva" name="description" />
    <!-- App favicon -->
    <link rel="shortcut icon" href="/assets/images/favicon.ico">

    <!-- jquery.vectormap css -->
    <link href="/assets/libs/admin-resources/jquery.vectormap/jquery-jvectormap-1.2.2.css" rel="stylesheet" type="text/css" />

    <!-- Bootstrap Css -->
    <link href="/assets/css/bootstrap.min.css" id="bootstrap-style" rel="stylesheet" type="text/css" />
    <!-- Icons Css -->
    <link href="/assets/css/icons.min.css" rel="stylesheet" type="text/css" />
    <!-- App Css-->
    <link href="/assets/css/app.min.css" id="app-style" rel="stylesheet" type="text/css" />

    @yield('css_custom')

</head>

<body data-layout="detached" data-topbar="colored">

    <div class="container-fluid">
        <!-- Begin page -->
        <div id="layout-wrapper">

            <header id="page-topbar">
                <div class="navbar-header">
                    <div class="container-fluid">
                        <div class="float-right">

                            <div class="dropdown d-inline-block d-lg-none ml-2">
                                <button type="button" class="btn header-item noti-icon waves-effect" id="page-header-search-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="mdi mdi-magnify"></i>
                                </button>
                                <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right p-0" aria-labelledby="page-header-search-dropdown">

                                    <form class="p-3">
                                        <div class="form-group m-0">
                                            <div class="input-group">
                                                <input type="text" class="form-control" placeholder="Поиск ..." aria-label="Recipient's username">
                                                <div class="input-group-append">
                                                    <button class="btn btn-primary" type="submit"><i class="mdi mdi-magnify"></i></button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>

                            <div class="dropdown d-none d-sm-inline-block">
                                @if(app()->getLocale() == "ru")
                                <button type="button" class="btn header-item waves-effect" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <img class="" src="/assets/images/flags/russia.jpg" alt="Header Language" height="16"> <span class="align-middle">Русский</span>
                                </button>
                                @endif
                                @if(app()->getLocale() == "en")
                                <button type="button" class="btn header-item waves-effect" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <img class="" src="/assets/images/flags/us.jpg" alt="Header Language" height="16"> <span class="align-middle">English</span>
                                </button>
                                @endif
                                @if(app()->getLocale() == "kz")
                                <button type="button" class="btn header-item waves-effect" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <img class="" src="/assets/images/flags/kazakhstan.svg" alt="Header Language" height="16"> <span class="align-middle">Қазақша</span>
                                </button>
                                @endif
                                <div class="dropdown-menu dropdown-menu-right">

                                    <a href="javascript:void(0);" onClick="window.location.href='/local/kz'" class="dropdown-item notify-item">
                                        <img src="/assets/images/flags/kazakhstan.svg" alt="user-image" class="mr-1" height="12"> <span class="align-middle">Қазақша</span>
                                    </a>
                                    
                                    <a href="javascript:void(0);" onClick="window.location.href='/local/ru'" class="dropdown-item notify-item">
                                        <img src="/assets/images/flags/russia.jpg" alt="user-image" class="mr-1" height="12"> <span class="align-middle">Русский</span>
                                    </a>

                                    <!-- item-->
                                    <a href="javascript:void(0);" onClick="window.location.href='/local/en'" class="dropdown-item notify-item">
                                        <img src="/assets/images/flags/us.jpg" alt="user-image" class="mr-1" height="12"> <span class="align-middle">English</span>
                                    </a>

                                </div>
                            </div>

                            <div class="dropdown d-none d-lg-inline-block ml-1">
                                <button type="button" class="btn header-item noti-icon waves-effect" data-toggle="fullscreen">
                                    <i class="mdi mdi-fullscreen"></i>
                                </button>
                            </div>

                            @if(Auth::check())
                            <div class="dropdown d-inline-block">
                                <button type="button" class="btn header-item waves-effect" id="page-header-user-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <img class="rounded-circle header-profile-user" src="/assets/images/users/avatar-2.jpg" alt="Header Avatar">
                                    <span class="d-none d-xl-inline-block ml-1">{{Auth::user()->name}} {{Auth::user()->surname}}</span>
                                    <i class="mdi mdi-chevron-down d-none d-xl-inline-block"></i>
                                </button>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <!-- item-->
                                    <a class="dropdown-item" href="/cabinet"><i class="bx bx-user font-size-16 align-middle mr-1"></i> Профиль</a>
                                    <a onclick="event.preventDefault(); document.getElementById('logoutform').submit();" class="dropdown-item text-danger" href="#"><i class="bx bx-power-off font-size-16 align-middle mr-1 text-danger"></i>Выйти</a>
                                </div>
                            </div>
                            <form id="logoutform" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                            @else
                                <div class="dropdown d-inline-block">
                                <a href="/login"><button type="button" class="btn header-item waves-effect" id="page-header-user-dropdown">
                                    <span class="d-none d-xl-inline-block ml-1">Вход/Регистрация</span>
                                </button></a>
                            </div>
                            @endif

                        </div>
                        <div>
                            <!-- LOGO -->
                            <div class="navbar-brand-box">
                                <a href="/" class="logo logo-dark">
                                    <span class="logo-sm">
                                        <img src="/assets/images/logo-sm.png" alt="" height="20">
                                    </span>
                                    <span class="logo-lg">
                                        <img src="/assets/images/logo-dark.png" alt="" height="17">
                                    </span>
                                </a>

                                <a href="/" class="logo logo-light">
                                    <span class="logo-sm">
                                        <img src="/assets/images/logo-sm.png" alt="" height="20">
                                    </span>
                                    <span class="logo-lg">
                                        <img src="/assets/images/logo-light.png" alt="" height="19">
                                    </span>
                                </a>
                            </div>

                            <button type="button" class="btn btn-sm px-3 font-size-16 header-item toggle-btn waves-effect" id="vertical-menu-btn">
                                <i class="fa fa-fw fa-bars"></i>
                            </button>

                            <!-- App Search-->
                            <form class="app-search d-none d-lg-inline-block">
                                <div class="position-relative">
                                    <input type="text" class="form-control" placeholder="Поиск документов...">
                                    <span class="bx bx-search-alt"></span>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </header> <!-- ========== Left Sidebar Start ========== -->