
<div class="vertical-menu">

    <div class="h-100">
        <!--- Sidemenu -->
        <div id="sidebar-menu">
            <!-- Left Menu Start -->
            <ul class="metismenu list-unstyled" id="side-menu">
                <li class="menu-title">Меню</li>

                <li>
                    <a href="/project_forms_table"><button type="button" class="btn btn-success waves-effect waves-light">__('front_menu.create_project')</button></a>
                </li>
                
                @if(Auth::check())

                <li>
                    <a href="/mydocs" class="waves-effect">
                        <i class="mdi mdi-book"></i>
                        <span>__('front_menu.my_docs')</span>
                    </a>
                </li>
                <hr>
                @endif

                <li>
                    <a href="/" class="waves-effect">
                        <i class="mdi mdi-airplay"></i>
                        <span>__('front_menu.main_page')</span>
                    </a>
                </li>

                <li>
                    <a href="/page/1" class="waves-effect">
                        <i class="mdi mdi-calendar-text"></i>
                        <span>__('front_menu.about_company')</span>
                    </a>
                </li>

                <li>
                    <a href="javascript: void(0);" class="has-arrow waves-effect">
                        <i class="mdi mdi-inbox-full"></i>
                        <span>__('front_menu.uslugi')</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li><a href="/page/2">__('front_menu.corp_obuch')</a></li>
                        <li><a href="/page/3">__('front_menu.ind_obuch')</a></li>
                        <li><a href="/page/4">__('front_menu.mezhd_cert')</a></li>
                        <li><a href="/page/5">__('front_menu.outsource')</a></li>
                        <li><a href="/page/6">__('front_menu.soprov')</a></li>
                        <li><a href="/page/7">__('front_menu.consultation')</a></li>
                    </ul>
                </li>

                <li>
                    <a href="javascript: void(0);" class="has-arrow waves-effect">
                        <i class="mdi mdi-flip-horizontal"></i>
                        <span>__('front_menu.poleznoye')</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li><a href="/page/8">__('front_menu.ssylki')</a></li>
                        <li><a href="/page/9">__('front_menu.knigi')</a></li>
                        <li><a href="/page/10">__('front_menu.films')</a></li>
                    </ul>
                </li>

                <li>
                    <a href="/page/11" class="waves-effect">
                        <i class="mdi mdi-calendar-text"></i>
                        <span>__('front_menu.contacts')</span>
                    </a>
                </li>

                @if(Auth::check()!=true)
                    <li>
                    <a href="/login" class="waves-effect">
                        <i class="mdi mdi-calendar-text"></i>
                        <span>__('front_menu.auth_login')</span>
                    </a>
                    </li>
                @endif

            </ul>
        </div>
        <!-- Sidebar -->
    </div>
</div>
<!-- Left Sidebar End -->