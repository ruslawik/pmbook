@extends('front2.layouts.main_layout')
@section('content')
            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="main-content">

                <div class="page-content">

                    <!-- start page title -->
                    <div class="row">
                        <div class="col-12">
                            <div class="page-title-box d-flex align-items-center justify-content-between">
                                <h4 class="page-title mb-0 font-size-18">Добро пожаловать!</h4>

                                <div class="page-title-right">
                                    <ol class="breadcrumb m-0">
                                        <li class="breadcrumb-item active">Главная страница</li>
                                    </ol>
                                </div>

                            </div>
                        </div>
                    </div>
                    <!-- end page title -->

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card border border-primary">
                                <div class="card-header bg-transparent border-primary">
                                    <h5 class="my-0 text-primary"><i class="mdi mdi-bullseye-arrow mr-3"></i>{{$page[0]->name}}</h5>
                                </div>
                                <div class="card-body">
                                    <!--
                                    <h5 class="card-title mt-0">Автоматизация работы с документами проекта</h5>
                                    <p class="card-text"><i>Определение проекта
ISO 21500, пункт 3.2</i><br>
Проект состоит из уникального набора скоординированных и контролируемых  процессов, с датами начала и окончания,  и выполняемых для достижения целей проекта.<br>
<i>PMBOK</i> 4<br>
Проект представляет собой временное предприятие, предназначенное для создания уникального продукта, результат или услуги.
</p>

<h5 class="card-title mt-0">Пошагово заполняйте поля и получите готовые документы и графики</h5>
                                    <p class="card-text"><i>Вы сможете получить наглядные графики показателей проекта как указано ниже и скачать готовые документы</i>
                                        !-->
                                    {!!$page[0]->content!!}

                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- End Page-content -->
@endsection
