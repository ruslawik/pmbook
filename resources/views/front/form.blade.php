@extends('front2.layouts.main_layout')
@section('content')

	            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="main-content">

                <div class="page-content">

                    <!-- start page title -->
                    <div class="row">
                        <div class="col-12">
                            <div class="page-title-box d-flex align-items-center justify-content-between">
                                <h4 class="page-title mb-0 font-size-18">Создание документа</h4>

                                <div class="page-title-right">
                                    <ol class="breadcrumb m-0">
                                        <li class="breadcrumb-item"><a href="javascript: void(0);">{{$form[0]->form_name}}</a></li>
                                    </ol>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end page title -->

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="card-title">{{$form[0]->form_name}}</h4>
                                    <br>
                                    <form id="form-horizontal" class="form-horizontal form-wizard-wrapper" action="/project/form/{{$form[0]->id}}/save" method="POST">
                                        {{csrf_field()}}
                                        <input type="submit" value="Сохранить" class="btn btn-success float-right">
                                        <input type="hidden" name="form_id" value="{{$form[0]->id}}">
                                        <?php $l = 1;?>
                                        @foreach($form[0]->formSteps as $step)
                                        <h3>{{$step['name']}}</h3>
                                        <?php $l++; ?>
                                        <fieldset>
                                            {!!$step['description']!!}
                                            <?php $k=0;?>
                                            <div class="row">
                                            @foreach($step->stepFields as $field)
                                                @if($field['field_type'] == "text")
                                                <div class="col-md-12">
                                                    <div class="form-group row">
                                                        <label for="{{$field['name']}}" class="col-lg-2 col-form-label">{{$field['user_name']}}</label>
                                                        <div class="col-lg-10">
                                                            <input id="{{$field['name']}}" name="{{$field['name']}}" type="text" class="form-control">
                                                        </div>
                                                    </div>
                                                </div>
                                                @endif
                                                @if($field['field_type'] == "textarea")
                                                <div class="col-md-12">
                                                    <div class="form-group row">
                                                        <label for="{{$field['name']}}" class="col-lg-2 col-form-label">{{$field['user_name']}}</label>
                                                        <div class="col-lg-10">
                                                            <textarea rows=5 id="{{$field['name']}}" name="{{$field['name']}}" class="form-control"></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                @endif
                                                <?php 
                                                    $k++;
                                                    if($k%2==0 && $k!=(count($step->stepFields))){
                                                ?>
                                                    </div>
                                                    <div class="row">
                                                <?php
                                                    }
                                                    if($k==(count($step->stepFields))){
                                                ?>
                                                    </div>
                                                <?php
                                                    }
                                                ?>
                                            @endforeach
                                        </fieldset>
                                        @endforeach
                                        <input type="submit" value="Сохранить" class="btn btn-success float-right">
                                    </form>

                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end row -->

                </div>
                <!-- End Page-content -->

@endsection

@section('js_scripts')
    <script src="/assets/libs/jquery-steps/build/jquery.steps.min.js"></script>
    <script src="/assets/js/pages/form-wizard.init.js"></script>
@endsection
