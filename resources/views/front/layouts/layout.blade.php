@include('front.partials._header')
@include('front.partials._side_menu')

@yield('main_content')

@include('front.partials._footer')