@extends('front.layouts.layout')

@section('css_custom')
    <link rel="stylesheet" href="/assets/jquery.jOrgChart.css"/>
@endsection

@section('main_content')

	            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="main-content">

                <div class="page-content">

                    <!-- start page title -->
                    <div class="row">
                        <div class="col-12">
                            <div class="page-title-box d-flex align-items-center justify-content-between">
                                <h4 class="page-title mb-0 font-size-18">Создание документа</h4>

                                <div class="page-title-right">
                                    <ol class="breadcrumb m-0">
                                        <li class="breadcrumb-item"><a href="javascript: void(0);"></a></li>
                                    </ol>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end page title -->

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="card-title">Дерево</h4>
                                    <button class="btn btn-success">Скачать</button>
                                    <div id="chart" class="orgChart"></div>
                                    <ul id="org" style="display:none">
<li>
  Food
  <ul>
    <li>Beer</li>
    <li>Vegetables
      <ul>
        <li>Pumpkin</li>
        <li>Aubergine</li>
      </ul>
    </li>
    <li>Bread</li>
    <li>Chocolate
      <ul>
        <li>Topdeck</li>
        <li>Reese's Cups</li>
      </ul>
    </li>
  </ul>
</li>
</ul>

<br><br>
<embed src="/bloknot.pdf" width="800px" height="1000px" />

                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end row -->

                </div>
                <!-- End Page-content -->

@endsection

@section('js_scripts')
    <script src="/assets/jquery.jOrgChart.js"></script>
    <script src="/assets/prettify.js"></script>
    <script type="text/javascript" src="https://html2canvas.hertzen.com/dist/html2canvas.min.js"></script>
    <script>
        jQuery(document).ready(function() {
        $("#org").jOrgChart({
            chartElement : '#chart',
            dragAndDrop  : true
        });
    });

    function download(){
        html2canvas(document.querySelector("#chart")).then(canvas => {
            document.body.appendChild(canvas)
        });
    }

    </script>
@endsection
