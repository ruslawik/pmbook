@extends('front2.layouts.main_layout')
@section('content')

	            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="main-content">

                <div class="page-content">

                    <!-- start page title -->
                    <div class="row">
                        <div class="col-12">
                            <div class="page-title-box d-flex align-items-center justify-content-between">
                                <h4 class="page-title mb-0 font-size-18"><br></h4>

                                <div class="page-title-right">
                                    <ol class="breadcrumb m-0">
                                        <li class="breadcrumb-item"><a href="javascript: void(0);">Личный кабинет</a></li>
                                    </ol>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end page title -->

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <h4 class="card-title">Информация профиля</h4>
                                        </div>
                                    </div>
                                    <br>
                                    @if (Session::has('msg'))
                                    <div class="alert alert-success">
                                        <ul>
                                            <span>{{ Session::get('msg') }}</span>
                                        </ul>
                                    </div>
                                    @endif
                                    @if (Session::has('error'))
                                    <div class="alert alert-danger">
                                        <ul>
                                            <li>{{ Session::get('error') }}</li>
                                        </ul>
                                    </div>
                                    @endif
                                    <form id="form-horizontal" class="form-horizontal form-wizard-wrapper" action="/update_user_info" method="POST">
                                        <div class="row">
                                            {{csrf_field()}}
                                            <div class="col-md-6">
                                                    <div class="form-group row">
                                                        <label for="name" class="col-lg-3 col-form-label">Имя</label>
                                                        <div class="col-lg-9">
                                                            <input id="name" name="name" type="text" class="form-control" value="{{Auth::user()->name}}">
                                                        </div>
                                                    </div>
                                                </div>
                                            <div class="col-md-6">
                                                    <div class="form-group row">
                                                        <label for="surname" class="col-lg-3 col-form-label">Фамилия</label>
                                                        <div class="col-lg-9">
                                                            <input id="surname" name="surname" type="text" class="form-control" value="{{Auth::user()->surname}}">
                                                        </div>
                                                    </div>
                                                </div>
                                        </div>
                                         <div class="row">
                                            <div class="col-md-6">
                                                    <div class="form-group row">
                                                        <label for="email" class="col-lg-3 col-form-label">Email</label>
                                                        <div class="col-lg-9">
                                                            <input id="email" name="email" type="text" class="form-control" value="{{Auth::user()->email}}">
                                                        </div>
                                                    </div>
                                                </div>
                                            <div class="col-md-6">
                                                    <div class="form-group row">
                                                        <label for="phone" class="col-lg-3 col-form-label">Телефон</label>
                                                        <div class="col-lg-9">
                                                            <input id="phone" name="phone" type="text" class="form-control" value="{{Auth::user()->phone}}">
                                                        </div>
                                                    </div>
                                                </div>
                                        </div>
                                         <div class="row">
                                            <div class="col-md-6">
                                                    <div class="form-group row">
                                                        <label for="password" class="col-lg-3 col-form-label">Новый пароль</label>
                                                        <div class="col-lg-9">
                                                            <input id="password" name="password" type="text" class="form-control">
                                                        </div>
                                                    </div>
                                                </div>
                                        </div>
                                        <center><button class="btn btn-primary" type="submit">Обновить</button></center>
                                    </form>

                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end row -->

                </div>
                <!-- End Page-content -->

@endsection
