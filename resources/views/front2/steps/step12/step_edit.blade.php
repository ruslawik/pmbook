@extends('front2.layouts.main_layout')
@section('content')

	            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="main-content">

                <div class="page-content">

                    <!-- start page title -->
                    <div class="row">
                        <div class="col-12">
                            <div class="page-title-box d-flex align-items-center justify-content-between">
                                <h4 class="page-title mb-0 font-size-18">Редактирование документа <br>"{{$doc[0]->name}}" <i class="mdi mdi-pencil" onClick="show_form();" style="cursor:pointer;"></i></h4>

                                <div class="page-title-right">
                                    <ol class="breadcrumb m-0">
                                        <li class="breadcrumb-item"><a href="javascript: void(0);">Разработка расписания работ</a></li>
                                    </ol>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end page title -->
                    @if (Session::has('msg'))
                        <div class="alert alert-success">
                                {{ Session::get('msg') }}
                        </div>
                    @endif
                    @if (Session::has('error'))
                        <div class="alert alert-danger">
                            <ul>
                                <li>{{ Session::get('error') }}</li>
                            </ul>
                        </div>
                    @endif
                     <form lass="form-horizontal form-wizard-wrapper" action="/project/step/12/doc/{{$doc[0]->id}}/update_name" method="POST" id="doc_name_form" style="display: none;">
                                        @csrf
                                        <div class="row">
                                            <div class="form-group col-5">
                                                <input type="text" name="doc_name" class="form-control" value="{{$doc[0]->name}}">
                                            </div>
                                            <input style="height:38px;" type="submit" value="Сохранить" class="btn btn-success">
                                        </div>
                                </form>
                    <script src="https://cdn.dhtmlx.com/gantt/edge/dhtmlxgantt.js"></script>
                    <link href="https://cdn.dhtmlx.com/gantt/edge/dhtmlxgantt.css" rel="stylesheet">

                    <script src="https://export.dhtmlx.com/gantt/api.js"></script>
                    <a href="/project/step/12/download-excel/{{$doc_id}}"><button class="btn btn-success">Экспортировать в Excel</button></a>
                    <br><br>
                    <div id="gantt_here" style='overflow-x: scroll; width:100%; height:700px !important; overflow-x: scroll;'></div>
                    <input type="hidden" value="{{csrf_token()}}" id="csrf">
                    <input type="hidden" value="{{$doc_id}}" id="gen_doc_id">
                    <style>
                        .weekend{ background: #f4f7f4 !important;}
                    </style>
                </div>
                <!-- End Page-content -->

@endsection

@section('js_scripts')
    <script type="text/javascript">
    gantt.config.xml_date = "%Y-%m-%d %H:%i:%s";
    gantt.config.order_branch = true;
    gantt.config.order_branch_free = true;
    gantt.config.scales = [
        { unit: "month", step: 1, format: "%M %Y" }
    ];

    gantt.init("gantt_here");
    gantt.load("/project/step/12/get-gantt-json/{{$doc_id}}");
    var dp = new gantt.dataProcessor("/project/step/12/gantt/");
    dp.init(gantt);
    dp.setTransactionMode({
      mode: "REST",
      payload: { _token: $("#csrf").val(), gen_doc_id: $("#gen_doc_id").val() }
    }, true);

    function show_form(){
            $("#doc_name_form").toggle();
    }
</script>
@endsection
