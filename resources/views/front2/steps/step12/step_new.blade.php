@extends('front2.layouts.main_layout')
@section('content')

	            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="main-content">

                <div class="page-content">

                    <!-- start page title -->
                    <div class="row">
                        <div class="col-12">
                            <div class="page-title-box d-flex align-items-center justify-content-between">
                                <h4 class="page-title mb-0 font-size-18">Создание документа</h4>

                                <div class="page-title-right">
                                    <ol class="breadcrumb m-0">
                                        <li class="breadcrumb-item"><a href="javascript: void(0);">Разработка расписания работ</a></li>
                                    </ol>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end page title -->

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="card-title">Расписание проекта</h4>
                                    <br>
                                    <div class="alert alert-danger">Внимание! Для разработки расписания проекта необходимо обязательное прохождение ШАГА 6 (Создание иерархической структуры работ)</div>
                                    <hr>
                                    <p>Выберите список задач из Ваших документов</p>
                                    <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                        <thead>
                                            <tr>
                                                <th>ID документа</th>
                                                <th>Название документа из 6 шага</th>
                                                <th>Новый документ (расписание проекта)</th>
                                                <th>Дата разработки</th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            @foreach($all_docs as $doc)
                                            <tr>
                                                <td>{{$doc->id}}</td>
                                                <td>
                                                    {{$doc->name}}
                                                </td>
                                                <td>
                                                    <form id="form-horizontal" class="form-horizontal form-wizard-wrapper" action="/project/step/12/create-from-doc/{{$doc->id}}" method="POST">
                                                        {{csrf_field()}}
                                                        <fieldset>
                                                                <div class="form-group">
                                                                    <label for="gen_doc_name">Введите название документа:</label>
                                                                    <input type="text" class="form-control" id="gen_doc_name" aria-describedby="gen_doc_help" value="{{$doc->name}}" name="gen_doc_name">
                                                                    <small id="gen_doc_help" class="form-text text-muted">Вы сможете в любое время получить доступ к нему в разделе "Мои документы"</small>
                                                                </div>
                                                                <button type="submit" class="btn btn-success">Создать документ</button>
                                                        </fieldset>
                                                    </form>
                                                </td>
                                                <td>{{$doc->created_at}}</td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end row -->

                </div>
                <!-- End Page-content -->

@endsection

@section('js_scripts')
    <script src="/assets/libs/jquery-steps/build/jquery.steps.min.js"></script>
    <script src="/assets/js/pages/form-wizard.init.js"></script>
@endsection
