
<form id="form-horizontal" class="form-horizontal form-wizard-wrapper" action="/project/step/14/save-statiya/{{$statiya[0]->id}}" method="POST">
    {{csrf_field()}}
    <fieldset>
        <div class="row">
            <input type="hidden" value="" name="time_in_project" class="form-control">
            <input type="hidden" value="" name="total_sum" class="form-control">
            <input type="hidden" value="" name="price_for_one" class="form-control">
            <input type="hidden" value="0" name="add_in_block" class="form-control">
            <div class="col-sm-6 input-group input-group-sm">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="inputGroup-sizing-sm">Название блока:</span>
                </div>
                <input type="text" value="{{$statiya[0]->statiya_name}}" name="statiya_name" class="form-control">
            </div>
        </div>
        <button type="submit" class="btn btn-sm btn-success mt-3">Сохранить</button>
        <a href="/project/step/14/delete-statiya/{{$statiya[0]->id}}" class="btn btn-danger mt-3" style="float:right;">Удалить БЛОК</a>
    </fieldset>
</form>