
<form id="form-horizontal" class="form-horizontal form-wizard-wrapper" action="/project/step/14/save-statiya/{{$statiya[0]->id}}" method="POST">
    {{csrf_field()}}
    <fieldset>
        <div class="row">
            <div class="col-sm-12 input-group input-group-sm">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="inputGroup-sizing-sm">В блок</span>
                </div>
                <select name="add_in_block" id="add_in_block" class="form-control">
                    @foreach($blocks as $block)
                        <option value="{{$block->id}}" @if($block->id == $statiya[0]->parent_id) selected @endif>{{$block->statiya_name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-sm-12 input-group input-group-sm mt-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="inputGroup-sizing-sm">Наименование статей затрат:</span>
                </div>
                <input type="text" value="{{$statiya[0]->statiya_name}}" name="statiya_name" class="form-control">
            </div>
            <div class="col-sm-12 input-group input-group-sm mt-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="inputGroup-sizing-sm">Стоимость за единицу:</span>
                </div>
                <input type="text" value="{{$statiya[0]->price_for_one}}" name="price_for_one" class="form-control">
            </div>
            <div class="col-sm-12 input-group input-group-sm mt-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="inputGroup-sizing-sm">Время ресурса на проекте:</span>
                </div>
                <input type="text" value="{{$statiya[0]->time_in_project}}" name="time_in_project" class="form-control">
            </div>
            <div class="col-sm-12 input-group input-group-sm mt-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="inputGroup-sizing-sm">Сумма:</span>
                </div>
                <input type="text" value="{{$statiya[0]->total_sum}}" name="total_sum" class="form-control">
            </div>
        </div>
        <button type="submit" class="btn btn-sm btn-success mt-3">Сохранить</button>
        <a href="/project/step/14/delete-statiya/{{$statiya[0]->id}}" class="btn btn-danger mt-3" style="float:right;">Удалить СТРОКУ</a>
    </fieldset>
</form>