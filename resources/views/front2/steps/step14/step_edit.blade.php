@extends('front2.layouts.main_layout')

@section('content')
<style>
    .statiya_class{
        background-color: #3cab94; padding:5px; color:white;
    }
    .blockiya_class{
        cursor: pointer;
    }
    .blockiya_class:hover{
        text-decoration: underline;
    }

</style>
<link href="/assets/8_step_style.css" rel="stylesheet" type="text/css">
    <div class="main-content">

                <div class="page-content">

                    <!-- start page title -->
                    <div class="row">
                        <div class="col-12">
                            <div class="page-title-box d-flex align-items-center justify-content-between">
                                <h4 class="page-title mb-0 font-size-18">Редактирование документа</h4>

                                <div class="page-title-right">
                                    <ol class="breadcrumb m-0">
                                        <li class="breadcrumb-item"><a href="javascript: void(0);">Разработка бюджета проекта</a></li>
                                    </ol>
                                </div>
                            </div>
                        </div>
                    </div>
                    @if (Session::has('msg'))
                                    <div class="alert alert-success">
                                        {!! Session::get('msg') !!}
                                    </div>
                                    @endif
                                    @if (Session::has('error'))
                                    <div class="alert alert-danger">
                                        <ul>
                                            <li>{{ Session::get('error') }}</li>
                                        </ul>
                                    </div>
                                    @endif
                    <!-- end page title -->
                            <div class="row">
                                    <div class="col-sm-12">
                                        <div class="card">
                                            <div class="card-body">
                                            <h4 class="card-title">{{$doc[0]->name}}
                                            <i class="mdi mdi-pencil" onClick="show_form();" style="cursor:pointer;"></i>&nbsp;<a href="/project/step/14/download/{{$doc[0]->id}}"><i class="mdi mdi-download"></i></a></h4>
                                            <form lass="form-horizontal form-wizard-wrapper" action="/project/step/14/doc/{{$doc[0]->id}}/update_name" method="POST" id="doc_name_form" style="display: none;">
                                        @csrf
                                        <div class="row">
                                            <div class="form-group col-5">
                                                <input type="text" name="doc_name" class="form-control" value="{{$doc[0]->name}}">
                                            </div>
                                            <input style="height:38px;" type="submit" value="Сохранить" class="btn btn-success">
                                        </div>
                                    </form>
                                            <hr>
                            <div class="row">

                                <div class="col-sm-12" id="new_stat_add">

                                    <form action="/project/step/14/add-statiya/{{$doc[0]->id}}" method="POST">
                                        @csrf
                                        <h5>Добавить <span id="statiya" class="statiya_class" onClick="show_statiya();">статью затрат</span>/<span id="blockiya" class="blockiya_class" onClick="show_statiya();">новый блок</span></h5>
                                        <div class="row">
                                        <div class="col-sm-4 input-group input-group-sm">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="inputGroup-sizing-sm">В блок</span>
                                            </div>
                                            <select name="add_in_block" id="add_in_block" class="form-control">
                                                @foreach($blocks as $block)
                                                    <option value="{{$block->id}}">{{$block->statiya_name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-sm-8 input-group input-group-sm">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="inputGroup-sizing-sm">Наименование статей затрат:</span>
                                            </div>
                                            <input type="text" name="statiya_name" class="form-control">
                                        </div>
                                        <div class="col-sm-4 input-group input-group-sm mt-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="inputGroup-sizing-sm">Стоимость за единицу:</span>
                                            </div>
                                            <input type="text" name="price_for_one" class="form-control">
                                        </div>
                                        <div class="col-sm-4 input-group input-group-sm mt-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="inputGroup-sizing-sm">Время ресурса на проекте:</span>
                                            </div>
                                            <input type="text" name="time_in_project" class="form-control">
                                        </div>
                                        <div class="col-sm-4 input-group input-group-sm mt-3">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="inputGroup-sizing-sm">Сумма:</span>
                                            </div>
                                            <input type="text" name="total_sum" class="form-control">
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-sm btn-success mt-3">Добавить</button>
                                    </form>

                                </div>
                                <div class="col-sm-12" id="new_block_add" style="display: none;">
                                    <form action="/project/step/14/add-block/{{$doc[0]->id}}" method="POST">
                                        @csrf
                                        <h5>Добавить <span id="statiya" class="blockiya_class" onClick="show_statiya();">статью затрат</span>/<span id="blockiya" class="statiya_class" onClick="show_statiya();">новый блок</span></h5>
                                        <div class="row">
                                            <div class="col-sm-6 input-group input-group-sm">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" id="inputGroup-sizing-sm">Название блока:</span>
                                                </div>
                                                <input type="text" name="block_name" class="form-control">
                                            </div>
                                        </div>
                                        <br>
                                        <button type="submit" class="btn btn-sm btn-success">Добавить</button>
                                    </form>
                                </div>

                            </div>
                                            <hr>
                            <div class="table__content" id="page-content">
                                <table style="width:100% !important;">
                                    <thead id="head_fixed">
                                    <tr>
                                        <th><b>№</b></th>
                                        <th><b>НАИМЕНОВАНИЕ СТАТЕЙ ЗАТРАТ</b></th>
                                        <th><b>СТОИМОСТЬ ЗА ЕДИНИЦУ</b></th>
                                        <th><b>ВРЕМЯ РЕСУРСА В ПРОЕКТЕ</b></th>
                                        <th><b>СУММА</b></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                                    $i = 1;
                                                ?>
                                        @foreach($blocks as $block)
                                        <tr onClick="edit_statiya('{{$block->id}}');">
                                            <th colspan="5" style="font-size:14 !important; text-align: center !important; background-color: #F4DD77;">
                                                {{$block->statiya_name}}
                                            </th>
                                        </tr>
                                            @if($block->hasChildren($block->id) == 1)
                                                @foreach($block->children($block->id) as $statiya)
                                                    <tr onClick="edit_statiya('{{$statiya->id}}');">
                                                        <td>{{$i}}</td>
                                                        <td>{{$statiya->statiya_name}}</td>
                                                        <td>{{$statiya->price_for_one}}</td>
                                                        <td>{{$statiya->time_in_project}}</td>
                                                        <td>{{$statiya->total_sum}}</td>
                                                    </tr>
                                                    <?php
                                                        $i++;
                                                    ?>
                                                @endforeach
                                            @endif
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>                                 

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- end page title end breadcrumb -->

                        </div><!-- container -->

                    </div> <!-- Page content Wrapper -->

                    <div class="modal fade edit-modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Редактировать</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                               <div id="editing_row">
                                   
                               </div>
                               <span id="loader" style="display: none;"><img src="/loader.gif" width=40px></span>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
                <input type="hidden" id="gen_doc_id" value="{{$doc[0]->id}}">
@endsection

@section('js_scripts')
    <script>

        function edit_statiya(statiya_id){
            var gen_doc_id = $("#gen_doc_id").val();
            $.ajax({
                type: "POST",
                url: "/project/step/14/ajax-get-statiya",
                beforeSend: function(){
                    $("#loader").toggle();
                },
                data: {_token: '{{csrf_token()}}', statiya_id: statiya_id, doc_id: gen_doc_id},
                success: function(answer) {
                    $("#loader").toggle();
                    $("#editing_row").html(answer);
                    $(".edit-modal").modal("show");
                },
                error: function(xhr){
                    $("#loader").toggle();
                    alert("Что-то пошло не так. Попробуйте еще раз либо обратитесь к техническому администратору. Ошибка: " + xhr.status + " " + xhr.statusText);
                }
            });
        }

        function show_statiya (){
            $("#new_stat_add").toggle();
            $("#new_block_add").toggle();
        }
        function show_form(){
            $("#doc_name_form").toggle();
        }
    </script>
@endsection