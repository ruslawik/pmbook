@extends('front2.layouts.main_layout')

@section('content')
<style>
    .statiya_class{
        background-color: #3cab94; padding:5px; color:white;
    }
    .blockiya_class{
        cursor: pointer;
    }
    .blockiya_class:hover{
        text-decoration: underline;
    }

</style>
<link href="/assets/8_step_style.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="/tabletree.css"></script>
    <div class="main-content">

                <div class="page-content">

                    <!-- start page title -->
                    <div class="row">
                        <div class="col-12">
                            <div class="page-title-box d-flex align-items-center justify-content-between">
                                <h4 class="page-title mb-0 font-size-18">Редактирование документа</h4>

                                <div class="page-title-right">
                                    <ol class="breadcrumb m-0">
                                        <li class="breadcrumb-item"><a href="javascript: void(0);">План качества проекта</a></li>
                                    </ol>
                                </div>
                            </div>
                        </div>
                    </div>
                    @if (Session::has('msg'))
                                    <div class="alert alert-success">
                                        {!! Session::get('msg') !!}
                                    </div>
                                    @endif
                                    @if (Session::has('error'))
                                    <div class="alert alert-danger">
                                        <ul>
                                            <li>{{ Session::get('error') }}</li>
                                        </ul>
                                    </div>
                                    @endif
                    <!-- end page title -->
                            <div class="row">
                                    <div class="col-sm-12">
                                        <div class="card">
                                            <div class="card-body">
                                            <h4 class="card-title">{{$doc[0]->name}}
                                            <i class="mdi mdi-pencil" onClick="show_form();" style="cursor:pointer;"></i>&nbsp;<a href="/project/step/16/download/{{$doc[0]->id}}"><i class="mdi mdi-download"></i></a></h4>
                                            <form lass="form-horizontal form-wizard-wrapper" action="/project/step/16/doc/{{$doc[0]->id}}/update_name" method="POST" id="doc_name_form" style="display: none;">
                                        @csrf
                                        <div class="row">
                                            <div class="form-group col-5">
                                                <input type="text" name="doc_name" class="form-control" value="{{$doc[0]->name}}">
                                            </div>
                                            <input style="height:38px;" type="submit" value="Сохранить" class="btn btn-success">
                                        </div>
                                    </form>
                                    <button class="btn btn-success" onClick="add_task_form();">Добавить задачу</button>
                            <hr>
                            <div class="table__content" id="page-content">
                                <table style="width:100% !important;" id="basic" class="table table-bordered table-striped">
                                    <thead id="head_fixed">
                                    <tr>
                                        <th><a href="#" onClick="open_close_all();"><b>Показать/<br>Скрыть все</b></a></th>
                                        <th><b>ЗАДАЧА</b></th>
                                        <th><b>ПОКАЗАТЕЛЬ</b></th>
                                        <th><b>ЕД. ИЗМ.</b></th>
                                        <th><b>КРИТЕРИИ</b></th>
                                        <th><b>ЦЕЛЕВ. ПОКАЗ.</b></th>
                                        <th><b>ЕД. ИЗМ.</b></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($tasks as $task)
                                            <tr data-node-id="{{$task->id}}" data-node-pid="{{$task->parent_id}}">
                                                <td></td>
                                                <td onClick="edit({{$task->id}}, {{$doc[0]->id}});">{{$task->task_text}}</td>
                                                <td onClick="edit({{$task->id}}, {{$doc[0]->id}});">{{$task->pokaz}}</td>
                                                <td onClick="edit({{$task->id}}, {{$doc[0]->id}});">{{$task->pokaz_ed_izm}}</td>
                                                <td onClick="edit({{$task->id}}, {{$doc[0]->id}});">{{$task->criteria}}</td>
                                                <td onClick="edit({{$task->id}}, {{$doc[0]->id}});">{{$task->goal_pokaz}}</td>
                                                <td onClick="edit({{$task->id}}, {{$doc[0]->id}});">{{$task->goal_pokaz_ed_izm}}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>                                 
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- end page title end breadcrumb -->

                        </div><!-- container -->

                    </div> <!-- Page content Wrapper -->

                    <div class="modal fade add-modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Добавить новую задачу</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <form action="/project/step/16/add-task/{{$doc[0]->id}}" method="POST">
                                    <input type="hidden" id="gen_doc_id" value="{{$doc[0]->id}}">
                                    {{csrf_field()}}
    <fieldset>
        <div class="row">
            <div class="col-sm-12 input-group input-group-sm">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="inputGroup-sizing-sm">Родительская задача:</span>
                </div>
                <select name="parent_id" class="form-control">
                    @foreach($tasks as $task_single)
                        <option value="{{$task_single->id}}">{{$task_single->task_text}}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-sm-12 input-group input-group-sm" style="margin-top:15px !important;">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="inputGroup-sizing-sm">Задача:</span>
                </div>
                <textarea name="task_text" class="form-control" rows=5></textarea>
            </div>
            <div class="col-sm-12 input-group input-group-sm" style="margin-top:15px !important;">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="inputGroup-sizing-sm">Критерии:</span>
                </div>
                <textarea name="criteria" class="form-control" rows=5></textarea>
            </div>
            <div class="col-sm-12 input-group input-group-sm mt-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="inputGroup-sizing-sm">Показатель:</span>
                </div>
                <input value="" type="text" name="pokaz" class="form-control">
            </div>
            <div class="col-sm-12 input-group input-group-sm mt-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="inputGroup-sizing-sm">Целевой показатель:</span>
                </div>
                <input value="" type="text" name="goal_pokaz" class="form-control">
            </div>
            <div class="col-sm-6 input-group input-group-sm mt-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="inputGroup-sizing-sm">Единица измерения показателя:</span>
                </div>
                <input value="" type="text" name="pokaz_ed_izm" class="form-control">
            </div>
            <div class="col-sm-6 input-group input-group-sm mt-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="inputGroup-sizing-sm">Единица измерения целевого показателя:</span>
                </div>
                <input value="" type="text" name="goal_pokaz_ed_izm" class="form-control">
            </div>
        </div>
        <button type="submit" class="btn btn-sm btn-success mt-3">Добавить задачу</button>
    </fieldset>
                                </form>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->



                <div class="modal fade edit-modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Редактировать</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                               <div id="editing_row">
                                   
                               </div>
                               <span id="loader" style="display: none;"><img src="/loader.gif" width=40px></span>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
@endsection

@section('js_scripts')
    <script src="/tabletree.js"></script>
    <script>
    function show_form(){
            $("#doc_name_form").toggle();
    }
    function add_task_form(){
            $(".add-modal").modal("show");
    }
    </script>
    <script>
    var data = sessionStorage.getItem('pkp_collapsed');
    if(data == "false"){
        $('#basic').simpleTreeTable({
            expander: $('#expander'),
            collapser: $('#collapser'),
            collapsed: false
        });
    }else{
        $('#basic').simpleTreeTable({
            expander: $('#expander'),
            collapser: $('#collapser'),
            storeState: true,
            collapsed: true
        });
    }
  
    function edit(task_id, gen_doc_id){

        $.ajax({
                type: "POST",
                url: "/project/step/16/ajax-get-task",
                beforeSend: function(){
                    $("#loader").toggle();
                },
                data: {_token: '{{csrf_token()}}', task_id: task_id, gen_doc_id: gen_doc_id},
                success: function(answer) {
                    $("#loader").toggle();
                    $("#editing_row").html(answer);
                    $(".edit-modal").modal("show");
                },
                error: function(xhr){
                    $("#loader").toggle();
                    alert("Что-то пошло не так. Попробуйте еще раз либо обратитесь к техническому администратору. Ошибка: " + xhr.status + " " + xhr.statusText);
                }
            });

    }

  function open_close_all(){
    var data = sessionStorage.getItem('pkp_collapsed');
    if(data == "false"){
        sessionStorage.setItem('pkp_collapsed', true);
    }else{
        sessionStorage.setItem('pkp_collapsed', false);
    }
    location.reload();
  }
  </script>
@endsection