<form id="form-horizontal" class="form-horizontal form-wizard-wrapper" action="/project/step/16/save-task/{{$task[0]->id}}" method="POST">
    {{csrf_field()}}
    <fieldset>
        <div class="row">
            @if($task[0]->parent_id == 0)
                <input type='hidden' name="parent_id" value=0>
            @else
            <div class="col-sm-12 input-group input-group-sm">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="inputGroup-sizing-sm">Родительская задача:</span>
                </div>
                <select name="parent_id" class="form-control">
                    @foreach($tasks as $task_single)
                        <option value="{{$task_single->id}}" @if($task[0]->parent_id == $task_single->id) selected @endif>{{$task_single->task_text}}</option>
                    @endforeach
                </select>
            </div>
            @endif
            <div class="col-sm-12 input-group input-group-sm" style="margin-top:15px !important;">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="inputGroup-sizing-sm">Задача:</span>
                </div>
                <textarea name="task_text" class="form-control" rows=5>{{$task[0]->task_text}}</textarea>
            </div>
            <div class="col-sm-12 input-group input-group-sm" style="margin-top:15px !important;">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="inputGroup-sizing-sm">Критерии:</span>
                </div>
                <textarea name="criteria" class="form-control" rows=5>{{$task[0]->criteria}}</textarea>
            </div>
            <div class="col-sm-12 input-group input-group-sm mt-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="inputGroup-sizing-sm">Показатель:</span>
                </div>
                <input value="{{$task[0]->pokaz}}" type="text" name="pokaz" class="form-control">
            </div>
            <div class="col-sm-12 input-group input-group-sm mt-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="inputGroup-sizing-sm">Целевой показатель:</span>
                </div>
                <input value="{{$task[0]->goal_pokaz}}" type="text" name="goal_pokaz" class="form-control">
            </div>
            <div class="col-sm-6 input-group input-group-sm mt-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="inputGroup-sizing-sm">Единица измерения показателя:</span>
                </div>
                <input value="{{$task[0]->pokaz_ed_izm}}" type="text" name="pokaz_ed_izm" class="form-control">
            </div>
            <div class="col-sm-6 input-group input-group-sm mt-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="inputGroup-sizing-sm">Единица измерения целевого показателя:</span>
                </div>
                <input value="{{$task[0]->goal_pokaz_ed_izm}}" type="text" name="goal_pokaz_ed_izm" class="form-control">
            </div>
        </div>
        <button type="submit" class="btn btn-sm btn-success mt-3">Сохранить</button>
        @if($task[0]->parent_id != 0)
            <a href="/project/step/16/delete-task/{{$task[0]->id}}" class="btn btn-danger mt-3" style="float:right;">Удалить задачу</a>
        @endif
    </fieldset>
</form>