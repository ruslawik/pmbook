@extends('front2.layouts.main_layout')

@section('content')
<style>
    #page-content {
        overflow-y: auto;
        height: 600px;
      }
    #page-content thead {
        position: sticky;
        top: 0;
      }
</style>
    <div class="main-content">

                <div class="page-content">

                    <!-- start page title -->
                    <div class="row">
                        <div class="col-12">
                            <div class="page-title-box d-flex align-items-center justify-content-between">
                                <h4 class="page-title mb-0 font-size-18">Редактирование документа</h4>

                                <div class="page-title-right">
                                    <ol class="breadcrumb m-0">
                                        <li class="breadcrumb-item"><a href="javascript: void(0);">Оценка ресурсов проекта</a></li>
                                    </ol>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end page title -->
                            <div class="row">
                                    <div class="col-sm-12">
                                        <div class="card">
                                            <div class="card-body">
                                            <h4 class="card-title">{{$doc_name}}
                                            <i class="mdi mdi-pencil" onClick="show_form();" style="cursor:pointer;"></i>&nbsp;<a href="/project/step/8/download/{{$gen_doc_id}}"><i class="mdi mdi-download"></i></a></h4>
                                    <form lass="form-horizontal form-wizard-wrapper" action="/project/step/8/doc/{{$gen_doc_id}}/update_name" method="POST" id="doc_name_form" style="display: none;">
                                        @csrf
                                        <div class="row">
                                            <div class="form-group col-5">
                                                <input type="text" name="doc_name" class="form-control" value="{{$doc_name}}">
                                            </div>
                                            <input style="height:38px;" type="submit" value="Сохранить" class="btn btn-success">
                                        </div>
                                    </form>
                                            <hr>
                                            Добавить строки в:
                                            <div class="row ml-1">
                                                <select id="row_id_to_add" class="form-control col-sm-5">
                                                    <option value="1">Человеческие ресурсы</option>
                                                    <option value="2">Оборудование</option>
                                                    <option value="3">Сырье/Материалы</option>
                                                    <option value="4">Информация и данные</option>
                                                    <option value="5">Услуги</option>
                                                    <option value="6">Расходные материалы</option>
                                                    <option value="7">Информационные и коммуникационные технологии (ИКТ)</option>
                                                    <option value="8">Транспорт и логистика</option>
                                                    <option value="9">ЗСП</option>
                                                    <option value="10">Финансы</option>
                                                </select>
                                                <button class="btn btn-success ml-3" onClick="add_row();">Добавить</button>
                                            </div>
                                            <hr>
                                            <div class="table__content" id="page-content">
                                                {!!$doc_html!!}
                                            </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- end page title end breadcrumb -->

                        </div><!-- container -->

                    </div> <!-- Page content Wrapper -->
                    <div class="modal fade edit-modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Редактировать</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                               <textarea id="editing_text" cols=53 rows=10></textarea>
                               <button class="btn btn-success" onClick="save();">Сохранить</button>
                               <span id="loader" style="display: none;"><img src="/loader.gif" width=40px></span>
                               <button class="btn btn-danger" style="float:right;" onClick="delete_row();">Удалить СТРОКУ</button>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
                <input type="hidden" id="gen_doc_id" value="{{$gen_doc_id}}">
@endsection

@section('js_scripts')
    <script>

        function add_row(){
            var row_id_to_add = $("#row_id_to_add").val();
            $("#row"+row_id_to_add).before("<tr><td>-</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>");
            var gen_doc_id = $("#gen_doc_id").val();
            save_html_to_database($("#page-content").html(), gen_doc_id);
            location.reload();
        }
        function delete_row (){
            $("#td1").parent().remove();
            var gen_doc_id = $("#gen_doc_id").val();
            save_html_to_database($("#page-content").html(), gen_doc_id);
        }
        function save(){
            var text_to_save = $("#editing_text").val();
            var gen_doc_id = $("#gen_doc_id").val();
            $("#td1").html(text_to_save);
            $("#td1").removeAttr("id");
            save_html_to_database($("#page-content").html(), gen_doc_id);
        }
        $("td:not(#add_row_td_to_click)").on("click", function(){
            $(".edit-modal").modal("show");
            $(".edit-modal textarea").val($(this).html());
            $(this).attr("id", "td1");
        });
        function save_html_to_database(html, gen_doc_id){
            $.ajax({
                type: "POST",
                url: "/project/step/8/save-html",
                beforeSend: function(){
                    $("#loader").toggle();
                },
                data: {_token: '{{csrf_token()}}', html: html, gen_doc_id: gen_doc_id},
                success: function(answer) {
                    $("#loader").toggle();
                    $(".edit-modal").modal("hide");
                },
                error: function(xhr){
                    $("#loader").toggle();
                    alert("Что-то пошло не так. Попробуйте еще раз либо обратитесь к техническому администратору. Ошибка: " + xhr.status + " " + xhr.statusText);
                }
            });
        }
        function show_form(){
            $("#doc_name_form").toggle();
        }
    </script>
@endsection