
<form id="form-horizontal" class="form-horizontal form-wizard-wrapper" action="/project/step/15/save-risk/{{$risk[0]->id}}" method="POST">
    {{csrf_field()}}
    <fieldset>
        <div class="row">
            <div class="col-sm-8 input-group input-group-sm">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="inputGroup-sizing-sm">Наименование риска:</span>
                </div>
                <input value="{{$risk[0]->name}}" type="text" name="name" class="form-control">
            </div>
            <div class="col-sm-4 input-group input-group-sm">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="inputGroup-sizing-sm">Стратегия:</span>
                </div>
                <select name="strat" id="strat" class="form-control">
                    <option value=0 @if($risk[0]->strat==0) selected @endif>Избегание</option>
                    <option value=1 @if($risk[0]->strat==1) selected @endif>Передача</option>
                    <option value=2 @if($risk[0]->strat==2) selected @endif>Смягчение</option>
                    <option value=3 @if($risk[0]->strat==3) selected @endif>Принятие</option>
                </select>
            </div>
            <div class="col-sm-5 input-group input-group-sm mt-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="inputGroup-sizing-sm">Вероятность наступления:</span>
                </div>
                <select name="ver" id="ver_modal" class="form-control" onChange="load_risk_img_modal();">
                    @for($i=1; $i<=5; $i++)
                        <option value={{$i}} @if($risk[0]->ver == $i) selected @endif>{{$i}}</option>
                    @endfor
                </select>
            </div>
            <div class="col-sm-4 input-group input-group-sm mt-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="inputGroup-sizing-sm">Уровень влияния:</span>
                </div>
                <select name="ur" id="ur_modal" class="form-control" onChange="load_risk_img_modal();">
                    @for($i=1; $i<=5; $i++)
                        <option value={{$i}} @if($risk[0]->ur == $i) selected @endif>{{$i}}</option>
                    @endfor
                </select>
            </div>
            <div class="col-sm-8 input-group input-group-sm mt-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="inputGroup-sizing-sm">Тактика:</span>
                </div>
                <textarea name="takt" class="form-control" rows=5>{{$risk[0]->takt}}</textarea>
            </div>
            <div class="col-sm-4 input-group input-group-sm mt-3">
                <p>Оценка риска:</p>
                <p id="risk_img_modal">
                </p>   
            </div>
        </div>
        <script>
            load_risk_img_modal();
        </script>
        <button type="submit" class="btn btn-sm btn-success mt-3">Сохранить</button>
        <a href="/project/step/15/delete-risk/{{$risk[0]->id}}" class="btn btn-danger mt-3" style="float:right;">Удалить РИСК</a>
    </fieldset>
</form>