@extends('front2.layouts.main_layout')

@section('content')
<style>
    .statiya_class{
        background-color: #3cab94; padding:5px; color:white;
    }
    .blockiya_class{
        cursor: pointer;
    }
    .blockiya_class:hover{
        text-decoration: underline;
    }

</style>
<link href="/assets/8_step_style.css" rel="stylesheet" type="text/css">
    <div class="main-content">

                <div class="page-content">

                    <!-- start page title -->
                    <div class="row">
                        <div class="col-12">
                            <div class="page-title-box d-flex align-items-center justify-content-between">
                                <h4 class="page-title mb-0 font-size-18">Редактирование документа</h4>

                                <div class="page-title-right">
                                    <ol class="breadcrumb m-0">
                                        <li class="breadcrumb-item"><a href="javascript: void(0);">Определение рисков</a></li>
                                    </ol>
                                </div>
                            </div>
                        </div>
                    </div>
                    @if (Session::has('msg'))
                                    <div class="alert alert-success">
                                        {!! Session::get('msg') !!}
                                    </div>
                                    @endif
                                    @if (Session::has('error'))
                                    <div class="alert alert-danger">
                                        <ul>
                                            <li>{{ Session::get('error') }}</li>
                                        </ul>
                                    </div>
                                    @endif
                    <!-- end page title -->
                            <div class="row">
                                    <div class="col-sm-12">
                                        <div class="card">
                                            <div class="card-body">
                                            <h4 class="card-title">{{$doc[0]->name}}
                                            <i class="mdi mdi-pencil" onClick="show_form();" style="cursor:pointer;"></i>&nbsp;<a href="/project/step/15/download/{{$doc[0]->id}}"><i class="mdi mdi-download"></i></a></h4>
                                            <form lass="form-horizontal form-wizard-wrapper" action="/project/step/15/doc/{{$doc[0]->id}}/update_name" method="POST" id="doc_name_form" style="display: none;">
                                        @csrf
                                        <div class="row">
                                            <div class="form-group col-5">
                                                <input type="text" name="doc_name" class="form-control" value="{{$doc[0]->name}}">
                                            </div>
                                            <input style="height:38px;" type="submit" value="Сохранить" class="btn btn-success">
                                        </div>
                                    </form>
                                            <hr>
                            <div class="row">

                                <div class="col-sm-12" id="new_stat_add">
                                    <h5>Добавить риск:</h5>
                                    <form action="/project/step/15/add-risk/{{$doc[0]->id}}" method="POST">
                                        @csrf
                                        <div class="row">
                                            <div class="col-sm-8 input-group input-group-sm">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" id="inputGroup-sizing-sm">Наименование риска:</span>
                                                </div>
                                                <input type="text" name="name" class="form-control">
                                            </div>
                                            <div class="col-sm-4 input-group input-group-sm">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" id="inputGroup-sizing-sm">Стратегия:</span>
                                                </div>
                                                <select name="strat" id="strat" class="form-control">
                                                    <option value=0>Избегание</option>
                                                    <option value=1>Передача</option>
                                                    <option value=2>Смягчение</option>
                                                    <option value=3>Принятие</option>
                                                </select>
                                            </div>
                                            <div class="col-sm-4 input-group input-group-sm mt-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" id="inputGroup-sizing-sm">Вероятность наступления:</span>
                                                </div>
                                                <select name="ver" id="ver" class="form-control" onChange="load_risk_img();">
                                                    <option value=1>1</option>
                                                    <option value=2>2</option>
                                                    <option value=3>3</option>
                                                    <option value=4>4</option>
                                                    <option value=5>5</option>
                                                </select>
                                            </div>
                                            <div class="col-sm-4 input-group input-group-sm mt-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" id="inputGroup-sizing-sm">Уровень влияния:</span>
                                                </div>
                                                <select name="ur" id="ur" class="form-control" onChange="load_risk_img();">
                                                    <option value=1>1</option>
                                                    <option value=2>2</option>
                                                    <option value=3>3</option>
                                                    <option value=4>4</option>
                                                    <option value=5>5</option>
                                                </select>
                                            </div>
                                            <div class="col-sm-8 input-group input-group-sm mt-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" id="inputGroup-sizing-sm">Тактика:</span>
                                                </div>
                                                <textarea name="takt" class="form-control" rows=5></textarea>
                                            </div>
                                            <div class="col-sm-4 input-group input-group-sm mt-3">
                                                <p>Оценка риска:</p>
                                                <p id="risk_img">
                                                </p>   
                                            </div>
                                            <div class="col-sm-12 mt-3">
                                                <button class="btn btn-success" type="submit">Добавить</button>
                                            </div>
                                        </div>
                                    </form>

                                </div>

                            </div>
                                            <hr>
                            <div class="table__content" id="page-content">
                                <table style="width:100% !important;">
                                    <thead id="head_fixed">
                                    <tr>
                                        <th><b>№</b></th>
                                        <th><b>НАИМЕНОВАНИЕ РИСКА</b></th>
                                        <th><b>ВЕРОЯТНОСТЬ НАСТУПЛЕНИЯ</b></th>
                                        <th><b>УРОВЕНЬ ВЛИЯНИЯ</b></th>
                                        <th><b>СТРАТЕГИЯ</b></th>
                                        <th><b>ТАКТИКА</b></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
                                            $i = 1;
                                        ?>
                                        @foreach($risks as $risk)
                                            <tr onClick="edit_risk({{$risk->id}});">
                                                <td rowspan=2>{{$i}}</td>
                                                <td rowspan=2>{{$risk->name}}</td>
                                                <td>{{$risk->ver}}</td>
                                                <td>{{$risk->ur}}</td>
                                                <td rowspan=2>{{$strats[$risk->strat]}}</td>
                                                <td rowspan=2>{{$risk->takt}}</td>
                                            </tr>
                                            <tr onClick="edit_risk({{$risk->id}});">
                                                <td colspan="2">
                                                    {{$risk->ver*$risk->ur}}
                                                    <br>
                                                    <img width=50% src="/step15-imgs/{{$risk->ver}}x{{$risk->ur}}.png">
                                                </td>
                                            </tr>
                                            <?php
                                                $i++;
                                            ?>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>                                 
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- end page title end breadcrumb -->

                        </div><!-- container -->

                    </div> <!-- Page content Wrapper -->

                    <div class="modal fade edit-modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Редактировать</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                               <div id="editing_row">
                                   
                               </div>
                               <span id="loader" style="display: none;"><img src="/loader.gif" width=40px></span>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
                <input type="hidden" id="gen_doc_id" value="{{$doc[0]->id}}">
@endsection

@section('js_scripts')
    <script>

        function load_risk_img(){
            var ver = $("#ver").val();
            var ur = $("#ur").val();
            $("#risk_img").html("<img width=55% src='/step15-imgs/"+ver+"x"+ur+".png'>");
        }

        function load_risk_img_modal(){
            var ver = $("#ver_modal").val();
            var ur = $("#ur_modal").val();
            $("#risk_img_modal").html("<img width=55% src='/step15-imgs/"+ver+"x"+ur+".png'>");
        }

        load_risk_img();

        function edit_risk(risk_id){
            $.ajax({
                type: "POST",
                url: "/project/step/15/ajax-get-risk",
                beforeSend: function(){
                    $("#loader").toggle();
                },
                data: {_token: '{{csrf_token()}}', risk_id: risk_id},
                success: function(answer) {
                    $("#loader").toggle();
                    $("#editing_row").html(answer);
                    $(".edit-modal").modal("show");
                },
                error: function(xhr){
                    $("#loader").toggle();
                    alert("Что-то пошло не так. Попробуйте еще раз либо обратитесь к техническому администратору. Ошибка: " + xhr.status + " " + xhr.statusText);
                }
            });
        }
        function show_form(){
            $("#doc_name_form").toggle();
        }
    </script>
@endsection