    <body class="fixed-left">

        <!-- Loader -->
        <div id="preloader"><div id="status"><div class="spinner"></div></div></div>

        <!-- Begin page -->
        <div id="wrapper">

            <!-- ========== Left Sidebar Start ========== -->
            <div class="left side-menu">
                <button type="button" class="button-menu-mobile button-menu-mobile-topbar open-left waves-effect">
                    <i class="ion-close"></i>
                </button>

                <!-- LOGO -->
                <div class="topbar-left" style="margin-top:-12px;">
                    <div class="text-center">
                        <a href="/" class="logo"><i class="mdi mdi-assistant"></i> PM.Book</a>
                        <!-- <a href="index.html" class="logo"><img src="assets/images/logo.png" height="24" alt="logo"></a> -->
                    </div>
                </div>

                <div class="sidebar-inner slimscrollleft">

                    <div id="sidebar-menu">
                        <ul>
                            <li class="menu-title">Меню</li>


                            <li>
                                <a href="/all-steps">
                                    <button type="button" class="btn btn-success waves-effect waves-light">{{__('front_menu.create_project')}}</button>
                                </a>
                            </li>

                            @if(Auth::check())

                            <li>
                                <a href="/mydocs" class="waves-effect">
                                    <i class="mdi mdi-book"></i>
                                    <span>{{__('front_menu.my_docs')}}</span>
                                </a>
                            </li>
                            <hr>
                            @endif

                            <li>
                                <a href="/" class="waves-effect">
                                    <i class="mdi mdi-airplay"></i>
                                    <span> {{__('front_menu.main_page')}}</span>
                                </a>
                            </li>

                            <li>
                                <a href="/page/1" class="waves-effect">
                                    <i class="mdi mdi-clipboard-outline"></i>
                                    <span> {{__('front_menu.about_company')}} </span> 
                                </a>
                            </li>

                             <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-cards-outline"></i> <span> {{__('front_menu.uslugi')}} </span> <span class="float-right"><i class="mdi mdi-chevron-right"></i></span></a>
                                <ul class="list-unstyled">
                                    <li><a href="/page/2">{{__('front_menu.corp_obuch')}}</a></li>
                                    <li><a href="/page/3">{{__('front_menu.ind_obuch')}}</a></li>
                                    <li><a href="/page/4">{{__('front_menu.mezhd_cert')}}</a></li>
                                    <li><a href="/page/5">{{__('front_menu.outsource')}}</a></li>
                                    <li><a href="/page/6">{{__('front_menu.soprov')}}</a></li>
                                    <li><a href="/page/7">{{__('front_menu.consultation')}}</a></li>
                                </ul>
                            </li>

                            <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-table"></i><span> {{__('front_menu.poleznoye')}} </span> <span class="float-right"><i class="mdi mdi-chevron-right"></i></span></a>
                                <ul class="list-unstyled">
                                    <li><a href="/page/8">{{__('front_menu.ssylki')}}</a></li>
                                    <li><a href="/page/9">{{__('front_menu.knigi')}}</a></li>
                                    <li><a href="/page/10">{{__('front_menu.films')}}</a></li>
                                </ul>
                            </li>

                             <li>
                                <a href="/page/11" class="waves-effect">
                                    <i class="mdi mdi-map-marker-multiple"></i>
                                    <span>{{__('front_menu.contacts')}}</span>
                                </a>
                            </li>

                            <li>
                                <a href="/page/51" class="waves-effect">
                                    <i class="mdi mdi-phone"></i>
                                    {{__('front_menu.take_consultation')}}
                                </a>
                            </li>

                            @if(Auth::check()!=true)
                            <li>
                                <a href="/login" class="waves-effect">
                                    <i class="mdi mdi-account-circle"></i>
                                    <span>{{__('front_menu.auth_login')}}</span>
                                </a>
                            </li>
                            @endif

                            

                        </ul>
                    </div>
                    <div class="clearfix"></div>
                </div> <!-- end sidebarinner -->
            </div>
            <!-- Left Sidebar End -->

            <div class="content-page" style="overflow: scroll !important;">
                <!-- Start content -->
                <div class="content">

                    <!-- Top Bar Start -->
                    <div class="topbar">

                        <nav class="navbar-custom">
                            <h5 id="header_title2">{{__('front_menu.bloknot_manager')}}</h5>

                            <ul class="list-inline float-right mb-0">
                                <!-- language-->
                                <li class="list-inline-item dropdown notification-list hide-phone">
                                    @if(app()->getLocale() == "ru")
                                        <a class="nav-link dropdown-toggle arrow-none waves-effect text-white" data-toggle="dropdown" href="#" role="button"
                                        aria-haspopup="false" aria-expanded="false">
                                        Русский <img src="/assets/images/flags/russia_flag.jpg" class="ml-2" height="16" alt=""/>
                                        </a>
                                    @endif
                                    @if(app()->getLocale() == "en")
                                        <a class="nav-link dropdown-toggle arrow-none waves-effect text-white" data-toggle="dropdown" href="#" role="button"
                                        aria-haspopup="false" aria-expanded="false">
                                        English <img src="/assets/images/flags/us_flag.jpg" class="ml-2" height="16" alt=""/>
                                        </a>
                                    @endif
                                    @if(app()->getLocale() == "kz")
                                        <a class="nav-link dropdown-toggle arrow-none waves-effect text-white" data-toggle="dropdown" href="#" role="button"
                                        aria-haspopup="false" aria-expanded="false">
                                        Қазақша <img src="/assets/images/flags/flag_kaz.svg" class="ml-2" height="16" alt=""/>
                                        </a>
                                    @endif
                                    <div class="dropdown-menu dropdown-menu-right language-switch">
                                        <a class="dropdown-item" href="/local/kz"><img src="/assets/images/flags/flag_kaz.svg" alt="" width="25"/><span> Қазақша </span></a>
                                        <a class="dropdown-item" href="/local/ru"><img src="/assets/images/flags/russia_flag.jpg" alt="" height="16"/><span> Русский </span></a>
                                        <a class="dropdown-item" href="/local/en"><img src="/assets/images/flags/us_flag.jpg" alt="" height="16"/><span> English </span></a>
                                    </div>
                                </li>

                                @if(Auth::check())
                                <li class="list-inline-item dropdown notification-list">
                                    <span style="color:white;">|</span>
                                    <a class="nav-link dropdown-toggle arrow-none waves-effect nav-user" data-toggle="dropdown" href="#" role="button"
                                       aria-haspopup="false" aria-expanded="false" style="color:white !important; text-decoration: none;">
                                        {{Auth::user()->name}} {{Auth::user()->surname}}
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right profile-dropdown ">
                                        <!-- item-->
                                        <a class="dropdown-item" href="/cabinet"><i class="mdi mdi-account-circle m-r-5 text-muted"></i> Профиль</a>
                                        <a class="dropdown-item" href="/mydocs"><i class="mdi mdi-book m-r-5 text-muted"></i>Мои документы</a>
                                        <a class="dropdown-item" href="/logout"><i class="mdi mdi-logout m-r-5 text-muted"></i> Выйти</a>
                                    </div>
                                </li>
                                @else
                                <li class="list-inline-item dropdown notification-list">
                                    <span style="color:white;">|</span>
                                    <a class="nav-link waves-effect nav-user" href="/login" style="color:white !important; text-decoration: none;">
                                        Вход
                                    </a>
                                </li>
                                @endif

                            </ul>

                            <ul class="list-inline menu-left mb-0">
                                <li class="float-left">
                                    <button class="button-menu-mobile open-left waves-light waves-effect">
                                        <i class="mdi mdi-menu"></i>
                                    </button>
                                </li>
                            </ul>

                            <div class="clearfix"></div>

                        </nav>

                    </div>
                    <!-- Top Bar End -->