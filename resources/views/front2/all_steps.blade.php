@extends('front2.layouts.main_layout')

@section('content')

<link href="/assets/all_steps_style.css" rel="stylesheet" type="text/css">
                    <div class="page-content-wrapper ">

                        <div class="container-fluid">

                            <div class="row">
                                    <div class="col-sm-12">
                                        <div class="page-title-box">


                                            <div class="table__content">
    <table>
        <thead>
        <tr>
            <th rowspan="2">{{__('all_steps.predmet')}}</th>
            <th colspan="5">{{__('all_steps.proc')}}</th>
        </tr>
        <tr>
            <th>{{__('all_steps.init')}}</th>
            <th>{{__('all_steps.plan')}}</th>
            <th>{{__('all_steps.exec')}}</th>
            <th>{{__('all_steps.control')}}</th>
            <th>{{__('all_steps.finish')}}</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <th rowspan="2">{{__('all_steps.integration')}}</th>
            <td><a href="/project/form/1">{{__('all_steps.s1')}}</a><br><a target="_blank" href="/page/13"><img src="/instruction.png" width=30></a></td>
            <td>{{__('all_steps.s4')}} <br><a target="_blank" href="/page/16"><img src="/instruction.png" width=30></a></td>
            <td>{{__('all_steps.s19')}}<br><a target="_blank" href="/page/31"><img src="/instruction.png" width=30></a></td>
            <td>{{__('all_steps.s26')}}<br><a target="_blank" href="/page/38"><img src="/instruction.png" width=30></a></td>
            <td>{{__('all_steps.s37')}}<br><a target="_blank" href="/page/49"><img src="/instruction.png" width=30></a></td>
        </tr>
        <tr>
            <td class="empty"></td>
            <td class="empty"></td>
            <td class="empty"></td>
            <td class="empty"></td>
            <td>{{__('all_steps.s38')}}<br><a target="_blank" href="/page/50"><img src="/instruction.png" width=30></a></td>
        </tr>
        <tr>
            <th>{{__('all_steps.zsp')}}</th>
            <td><a href="/project/form/3">{{__('all_steps.s2')}}</a><br><a target="_blank" href="/page/14"><img src="/instruction.png" width=30></a></td>
            <td class="empty"></td>
            <td>{{__('all_steps.s20')}}<br><a target="_blank" href="/page/32"><img src="/instruction.png" width=30></a></td>
            <td class="empty"></td>
            <td class="empty"></td>
        </tr>
        <tr>
            <th rowspan="3">{{__('all_steps.soder')}}</th>
            <td class="empty"></td>
            <td>{{__('all_steps.s5')}}<br><a target="_blank" href="/page/17"><img src="/instruction.png" width=30></a></td>
            <td class="empty"></td>
            <td>{{__('all_steps.s27')}}<br><a target="_blank" href="/page/39"><img src="/instruction.png" width=30></a></td>
            <td class="empty"></td>
        </tr>
        <tr>
            <td class="empty"></td>
            <td><a href="/project/step/6/new">{{__('all_steps.s6')}}</a><br><a target="_blank" href="/page/18"><img src="/instruction.png" width=30></a></td>
            <td class="empty"></td>
            <td>{{__('all_steps.s28')}}<br><a target="_blank" href="/page/40"><img src="/instruction.png" width=30></a></td>
            <td class="empty"></td>
        </tr>
        <tr>
            <td class="empty"></td>
            <td>{{__('all_steps.s7')}}<br><a target="_blank" href="/page/19"><img src="/instruction.png" width=30></a></td>
            <td class="empty"></td>
            <td class="empty"></td>
            <td class="empty"></td>
        </tr>
        <tr>
            <th rowspan="2">{{__('all_steps.res')}}</th>
            <td><a href="/project/form/5">{{__('all_steps.s3')}}</a><br><a target="_blank" href="/page/15"><img src="/instruction.png" width=30></a></td>
            <td><a href="/project/step/8/new">{{__('all_steps.s8')}}</a><br><a target="_blank" href="/page/20"><img src="/instruction.png" width=30></a></td>
            <td>{{__('all_steps.s21')}}<br><a target="_blank" href="/page/33"><img src="/instruction.png" width=30></a></td>
            <td>{{__('all_steps.s29')}}<br><a target="_blank" href="/page/41"><img src="/instruction.png" width=30></a></td>
            <td class="empty"></td>
        </tr>
        <tr>
            <td class="empty"></td>
            <td>{{__('all_steps.s9')}}<br><a target="_blank" href="/page/21"><img src="/instruction.png" width=30></a></td>
            <td class="empty"></td>
            <td>{{__('all_steps.s30')}}<br><a target="_blank" href="/page/42"><img src="/instruction.png" width=30></a></td>
            <td class="empty"></td>
        </tr>
        <tr>
            <th rowspan="3">{{__('all_steps.uakit')}}</th>
            <td class="empty"></td>
            <td>{{__('all_steps.s10')}}<br><a target="_blank" href="/page/22"><img src="/instruction.png" width=30></a></td>
            <td class="empty"></td>
            <td>{{__('all_steps.s31')}}<br><a target="_blank" href="/page/43"><img src="/instruction.png" width=30></a></td>
            <td class="empty"></td>
        </tr>
        <tr>
            <td class="empty"></td>
            <td>{{__('all_steps.s11')}}<br><a target="_blank" href="/page/23"><img src="/instruction.png" width=30></a></td>
            <td class="empty"></td>
            <td class="empty"></td>
            <td class="empty"></td>
        </tr>
        <tr>
            <td class="empty"></td>
            <td><a href="/project/step/12/new">{{__('all_steps.s12')}}</a><br><a target="_blank" href="/page/24"><img src="/instruction.png" width=30></a></td>
            <td class="empty"></td>
            <td class="empty"></td>
            <td class="empty"></td>
        </tr>
        <tr>
            <th rowspan="2">{{__('all_steps.stoim')}}</th>
            <td class="empty"></td>
            <td>{{__('all_steps.s13')}}<br><a target="_blank" href="/page/25"><img src="/instruction.png" width=30></a></td>
            <td class="empty"></td>
            <td>{{__('all_steps.s32')}}<br><a target="_blank" href="/page/44"><img src="/instruction.png" width=30></a></td>
            <td class="empty"></td>
        </tr>
        <tr>
            <td class="empty"></td>
            <td><a href="/project/step/14/new">{{__('all_steps.s14')}}</a><br><a target="_blank" href="/page/26"><img src="/instruction.png" width=30></a></td>
            <td class="empty"></td>
            <td class="empty"></td>
            <td class="empty"></td>
        </tr>
        <tr>
            <th>{{__('all_steps.riski')}}</th>
            <td class="empty"></td>
            <td><a href="/project/step/15/new">{{__('all_steps.s15')}}</a><br><a target="_blank" href="/page/27"><img src="/instruction.png" width=30></a></td>
            <td>{{__('all_steps.s22')}}<br><a target="_blank" href="/page/34"><img src="/instruction.png" width=30></a></td>
            <td>{{__('all_steps.s33')}}<br><a target="_blank" href="/page/45"><img src="/instruction.png" width=30></a></td>
            <td class="empty"></td>
        </tr>
        <tr>
            <th>{{__('all_steps.ka4est')}}</th>
            <td class="empty"></td>
            <td><a href="/project/step/16/new">{{__('all_steps.s16')}}</a><br><a target="_blank" href="/page/28"><img src="/instruction.png" width=30></a></td>
            <td>{{__('all_steps.s23')}}<br><a target="_blank" href="/page/35"><img src="/instruction.png" width=30></a></td>
            <td>{{__('all_steps.s34')}}<br><a target="_blank" href="/page/46"><img src="/instruction.png" width=30></a></td>
            <td class="empty"></td>
        </tr>
        <tr>
            <th>{{__('all_steps.postav')}}</th>
            <td class="empty"></td>
            <td>{{__('all_steps.s17')}}<br><a target="_blank" href="/page/29"><img src="/instruction.png" width=30></a></td>
            <td>{{__('all_steps.s24')}}<br><a target="_blank" href="/page/36"><img src="/instruction.png" width=30></a></td>
            <td>{{__('all_steps.s35')}}<br><a target="_blank" href="/page/47"><img src="/instruction.png" width=30></a></td>
            <td class="empty"></td>
        </tr>
        <tr>
            <th>{{__('all_steps.finish')}}</th>
            <td class="empty"></td>
            <td>{{__('all_steps.s18')}}<br><a target="_blank" href="/page/30"><img src="/instruction.png" width=30></a></td>
            <td>{{__('all_steps.s25')}}<br><a target="_blank" href="/page/37"><img src="/instruction.png" width=30></a></td>
            <td>{{__('all_steps.s36')}}<br><a target="_blank" href="/page/48"><img src="/instruction.png" width=30></a></td>
            <td class="empty"></td>
        </tr>
        </tbody>
    </table>
</div>

                                        </div>
                                    </div>
                                </div>
                                <!-- end page title end breadcrumb -->

                        </div><!-- container -->

                    </div> <!-- Page content Wrapper -->
@endsection