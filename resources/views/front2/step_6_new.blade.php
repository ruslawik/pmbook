@extends('front2.layouts.main_layout')
@section('content')
<link href="/assets/all_steps_style.css" rel="stylesheet" type="text/css">

	            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="main-content">

                <div class="page-content">

                    <!-- start page title -->
                    <div class="row">
                        <div class="col-12">
                            <div class="page-title-box d-flex align-items-center justify-content-between">
                                <h4 class="page-title mb-0 font-size-18">Создание документа</h4>

                                <div class="page-title-right">
                                    <ol class="breadcrumb m-0">
                                        <li class="breadcrumb-item"><a href="javascript: void(0);">Создание иерархической структуры работ</a></li>
                                    </ol>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end page title -->

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="card-title">Создание иерархической структуры работ</h4>
                                    <br>
                                    <form id="form-horizontal" class="form-horizontal form-wizard-wrapper" action="/project/step_6/create_new" method="POST">
                                        {{csrf_field()}}
                                        <fieldset>
                                                <div class="form-group">
                                                    <label for="gen_doc_name">Введите название документа:</label>
                                                    <input type="text" class="form-control" id="gen_doc_name" aria-describedby="gen_doc_help" placeholder="Например: Документ проекта 'Проект года' 2021" name="gen_doc_name">
                                                    <small id="gen_doc_help" class="form-text text-muted">Вы сможете в любое время получить доступ к нему в разделе "Мои документы"</small>
                                                </div>
                                                <button type="submit" class="btn btn-success">Создать документ</button>
                                        </fieldset>
                                    </form>
                                    <hr>
                                    <ul class="tree">
                                        <li> <span>Результат работ</span>
                                            <ul>
                                                <li><span>Задача 1</span>
                                                    <ul>
                                                        <li><span>Подзадача 1.1</span></li>
                                                        <li><span>Подзадача 1.2</span></li>
                                                    </ul>
                                                </li>
                                                <li><span>Задача 2</span>
                                                    <ul>
                                                        <li><span>Подзадача 2.1</span></li>
                                                        <li><span>Подзадача 2.2</span></li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end row -->

                </div>
                <!-- End Page-content -->

@endsection

@section('js_scripts')
    <script src="/assets/libs/jquery-steps/build/jquery.steps.min.js"></script>
    <script src="/assets/js/pages/form-wizard.init.js"></script>
@endsection
