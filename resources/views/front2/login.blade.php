@extends('front2.layouts.main_layout')

@section('content')


                    <div class="page-content-wrapper ">

                        <div class="container-fluid">

                            <div class="row">
                                    <div class="col-sm-12">
                                        <div class="page-title-box">


                                            <div class="table__content">
    <table>
        <thead>
        <tr>
            <th rowspan="2">Предметные группы</th>
            <th colspan="5">Группы процессов</th>
        </tr>
        <tr>
            <th>Инициирование</th>
            <th>Планирование</th>
            <th>Исполнение</th>
            <th>Контроль</th>
            <th>Завершение</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <th rowspan="2">Интеграция</th>
            <td>Шаг 1. Разработка устава проекта</td>
            <td>Шаг 4. Разработка планов проектов</td>
            <td>Шаг 19. Исполнение работ</td>
            <td>Шаг 26. Контроль исполнения</td>
            <td>Шаг 37. Закрытие фазы или проекта</td>
        </tr>
        <tr>
            <td class="empty"></td>
            <td class="empty"></td>
            <td class="empty"></td>
            <td class="empty"></td>
            <td>Шаг 38. Извлеченные уроки/аудит</td>
        </tr>
        <tr>
            <th>Заинтересованные стороны (ЗСП)</th>
            <td>Шаг 2. Определение ЗСП</td>
            <td class="empty"></td>
            <td>Шаг 20. Управление ЗСП</td>
            <td class="empty"></td>
            <td class="empty"></td>
        </tr>
        <tr>
            <th rowspan="3">Содержание</th>
            <td class="empty"></td>
            <td>Шаг 5. Определение содержания проекта</td>
            <td class="empty"></td>
            <td>Шаг 27. Управление изменениями</td>
            <td class="empty"></td>
        </tr>
        <tr>
            <td class="empty"></td>
            <td>Шаг 6. Создание иерархической структуры работ</td>
            <td class="empty"></td>
            <td>Шаг 28. Контроль содержания</td>
            <td class="empty"></td>
        </tr>
        <tr>
            <td class="empty"></td>
            <td>Шаг 7. Определение операций</td>
            <td class="empty"></td>
            <td class="empty"></td>
            <td class="empty"></td>
        </tr>
        <tr>
            <th rowspan="2">Ресурсы</th>
            <td>Шаг 3. Создание команды проекта</td>
            <td>Шаг 8. Оценка ресурсов</td>
            <td>Шаг 21. Развитие команды проекта</td>
            <td>Шаг 29. Контроль ресурсов</td>
            <td class="empty"></td>
        </tr>
        <tr>
            <td class="empty"></td>
            <td>Шаг 9. Определение организационной структуры работ</td>
            <td class="empty"></td>
            <td>Шаг 30. Контроль команды</td>
            <td class="empty"></td>
        </tr>
        <tr>
            <th rowspan="3">Время</th>
            <td class="empty"></td>
            <td>Шаг 10. Последовательность работ</td>
            <td class="empty"></td>
            <td>Шаг 31. Контроль расписания</td>
            <td class="empty"></td>
        </tr>
        <tr>
            <td class="empty"></td>
            <td>Шаг 11. Оценка длительности работ</td>
            <td class="empty"></td>
            <td class="empty"></td>
            <td class="empty"></td>
        </tr>
        <tr>
            <td class="empty"></td>
            <td>Шаг 12. Разработка расписания работ</td>
            <td class="empty"></td>
            <td class="empty"></td>
            <td class="empty"></td>
        </tr>
        <tr>
            <th rowspan="2">Стоимость</th>
            <td class="empty"></td>
            <td>Шаг 13. Оценка затрат</td>
            <td class="empty"></td>
            <td>Шаг 32. Контроль стоимости</td>
            <td class="empty"></td>
        </tr>
        <tr>
            <td class="empty"></td>
            <td>Шаг 14. Разработка бюджета</td>
            <td class="empty"></td>
            <td class="empty"></td>
            <td class="empty"></td>
        </tr>
        <tr>
            <th>Риски</th>
            <td class="empty"></td>
            <td>Шаг 15. Определение рисков</td>
            <td>Шаг 22. Управление рисками</td>
            <td>Шаг 33. Контроль рисков</td>
            <td class="empty"></td>
        </tr>
        <tr>
            <th>Качество</th>
            <td class="empty"></td>
            <td>Шаг 16. Создание плана по качеству</td>
            <td>Шаг 23. Обеспечение требований качества</td>
            <td>Шаг 34. Контроль качества</td>
            <td class="empty"></td>
        </tr>
        <tr>
            <th>Поставки</th>
            <td class="empty"></td>
            <td>Шаг 17. План поставок</td>
            <td>Шаг 24. Выбор поставщиков</td>
            <td>Шаг 35. Администрирование контрактов</td>
            <td class="empty"></td>
        </tr>
        <tr>
            <th>Коммуникации</th>
            <td class="empty"></td>
            <td>Шаг 18. План коммуникаций</td>
            <td>Шаг 25. Управление коммуникациями</td>
            <td>Шаг 36. Контроль коммуникаций</td>
            <td class="empty"></td>
        </tr>
        </tbody>
    </table>
</div>

                                        </div>
                                    </div>
                                </div>
                                <!-- end page title end breadcrumb -->

                        </div><!-- container -->

                    </div> <!-- Page content Wrapper -->
@endsection