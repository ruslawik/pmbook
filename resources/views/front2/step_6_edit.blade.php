@extends('front2.layouts.main_layout')
@section('content')
<link href="/assets/all_steps_style.css" rel="stylesheet" type="text/css">

	            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="main-content">

                <div class="page-content">

                    <!-- start page title -->
                    <div class="row">
                        <div class="col-12">
                            <div class="page-title-box d-flex align-items-center justify-content-between">
                                <h4 class="page-title mb-0 font-size-18">Редактирование документа</h4>

                                <div class="page-title-right">
                                    <ol class="breadcrumb m-0">
                                        <li class="breadcrumb-item"><a href="javascript: void(0);">Создание иерархической структуры работ</a></li>
                                    </ol>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end page title -->

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="card-title">{{$doc[0]->name}} <i class="mdi mdi-pencil" onClick="show_form();" style="cursor:pointer;"></i>&nbsp;<a href="/download/step-6/{{$doc[0]->id}}"><i class="mdi mdi-download"></i></a></h4>
                                    <form lass="form-horizontal form-wizard-wrapper" action="/project/step/6/doc/{{$doc[0]->id}}/update_name" method="POST" id="doc_name_form" style="display: none;">
                                        @csrf
                                        <div class="row">
                                            <div class="form-group col-5">
                                                <input type="text" name="doc_name" class="form-control" value="{{$doc[0]->name}}">
                                            </div>
                                            <input style="height:38px;" type="submit" value="Сохранить" class="btn btn-success">
                                        </div>
                                        <hr>
                                    </form>
                                    <br>
                                    <form id="form-horizontal" class="form-horizontal form-wizard-wrapper" action="/project/step/6/doc/{{$doc[0]->id}}/add" method="POST">
                                        {{csrf_field()}}
                                        <fieldset>
                                            <div class="row">
                                                <div class="form-group col-5">
                                                    <input type="hidden" name="gen_doc_id" value="{{$doc[0]->id}}">
                                                    <label for="root_element">Выберите родительский элемент:</label>
                                                    <select class="form-control" name="root_element" id="root_element">
                                                        @foreach($all_tasks as $task)
                                                            @if(isset($_GET['root']) && $_GET['root']==$task->id)
                                                                <option value="{{$task->id}}" selected="selected">{{$task->name}}</option>
                                                            @else
                                                                <option value="{{$task->id}}">{{$task->name}}</option>
                                                            @endif
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="form-group col-5">
                                                    <label for="subtask">Добавить подзадачу</label>
                                                    <input type="text" class="form-control" id="subtask" name="subtask" autofocus>
                                                </div>
                                                <button style="height:40px;margin-top:28px;" type="submit" class="btn btn-success col-sm-2">Добавить</button>
                                            </div>
                                                
                                        </fieldset>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br>
                    <br>
                    <ul class="tree">
                        {!!$tree_html!!}
                    </ul>
                    <!-- end row -->

                </div>
                <!-- End Page-content -->

@endsection

@section('js_scripts')
    <script src="/assets/libs/jquery-steps/build/jquery.steps.min.js"></script>
    <script src="/assets/js/pages/form-wizard.init.js"></script>
    <script>
        function show_form(){
            $("#doc_name_form").toggle();
        }
        function edit(node_id){
            var html = $("#node"+node_id).html();
            if(html.includes("textarea class=")){

            }else{
                var q = "'";
                $("#node"+node_id).html('<form action="/project/step6/node/" method="POST"> @csrf <input type="hidden" name="node_id" value="'+node_id+'"><textarea class="form-control" name="node_text">'+html+'</textarea><button type="submit" class="btn btn-sm btn-success">Сохранить</button></form><a href="/project/step6/node/'+node_id+'/delete"><button class="btn btn-sm btn-danger">Удалить</button></a>');
            }
        }


    </script> 
@endsection
