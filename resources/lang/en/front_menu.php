<?php
return[
     'main_page' => 'Main',
     'about_company' => 'About me',
     'uslugi' => 'Services',
     'poleznoye' => 'Useful',
     'contacts' => 'Contacts',
     'corp_obuch' => 'Corporate training',
     'ind_obuch' => 'Individual training',
     'mezhd_cert' => 'International certification',
     'outsource' => 'Project management outsourcing',
     'soprov' => 'Project support',
     'consultation' => 'Consultation',
     'ssylki' => 'Links',
     'books' => 'Books',
     'films' => 'Movies',
     'my_docs' => 'My documents',
     'create_project' => 'Create your own project!',
     'auth_login' => 'Login/Register',
     'bloknot_manager' => "Project Manager's Notebook",
];
?>