<?php

use App\Models\Permission;
use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    public function run()
    {
        $permissions = [
            [
                'id'    => 1,
                'title' => 'user_management_access',
            ],
            [
                'id'    => 2,
                'title' => 'permission_create',
            ],
            [
                'id'    => 3,
                'title' => 'permission_edit',
            ],
            [
                'id'    => 4,
                'title' => 'permission_show',
            ],
            [
                'id'    => 5,
                'title' => 'permission_delete',
            ],
            [
                'id'    => 6,
                'title' => 'permission_access',
            ],
            [
                'id'    => 7,
                'title' => 'role_create',
            ],
            [
                'id'    => 8,
                'title' => 'role_edit',
            ],
            [
                'id'    => 9,
                'title' => 'role_show',
            ],
            [
                'id'    => 10,
                'title' => 'role_delete',
            ],
            [
                'id'    => 11,
                'title' => 'role_access',
            ],
            [
                'id'    => 12,
                'title' => 'user_create',
            ],
            [
                'id'    => 13,
                'title' => 'user_edit',
            ],
            [
                'id'    => 14,
                'title' => 'user_show',
            ],
            [
                'id'    => 15,
                'title' => 'user_delete',
            ],
            [
                'id'    => 16,
                'title' => 'user_access',
            ],
            [
                'id'    => 17,
                'title' => 'form_create',
            ],
            [
                'id'    => 18,
                'title' => 'form_edit',
            ],
            [
                'id'    => 19,
                'title' => 'form_show',
            ],
            [
                'id'    => 20,
                'title' => 'form_delete',
            ],
            [
                'id'    => 21,
                'title' => 'form_access',
            ],
            [
                'id'    => 22,
                'title' => 'step_create',
            ],
            [
                'id'    => 23,
                'title' => 'step_edit',
            ],
            [
                'id'    => 24,
                'title' => 'step_show',
            ],
            [
                'id'    => 25,
                'title' => 'step_delete',
            ],
            [
                'id'    => 26,
                'title' => 'step_access',
            ],
            [
                'id'    => 27,
                'title' => 'field_create',
            ],
            [
                'id'    => 28,
                'title' => 'field_edit',
            ],
            [
                'id'    => 29,
                'title' => 'field_show',
            ],
            [
                'id'    => 30,
                'title' => 'field_delete',
            ],
            [
                'id'    => 31,
                'title' => 'field_access',
            ],
            [
                'id'    => 32,
                'title' => 'page_create',
            ],
            [
                'id'    => 33,
                'title' => 'page_edit',
            ],
            [
                'id'    => 34,
                'title' => 'page_show',
            ],
            [
                'id'    => 35,
                'title' => 'page_delete',
            ],
            [
                'id'    => 36,
                'title' => 'page_access',
            ],
            [
                'id'    => 37,
                'title' => 'form_constructor_access',
            ],
            [
                'id'    => 38,
                'title' => 'gen_doc_create',
            ],
            [
                'id'    => 39,
                'title' => 'gen_doc_edit',
            ],
            [
                'id'    => 40,
                'title' => 'gen_doc_show',
            ],
            [
                'id'    => 41,
                'title' => 'gen_doc_delete',
            ],
            [
                'id'    => 42,
                'title' => 'gen_doc_access',
            ],
            [
                'id'    => 43,
                'title' => 'answer_create',
            ],
            [
                'id'    => 44,
                'title' => 'answer_edit',
            ],
            [
                'id'    => 45,
                'title' => 'answer_show',
            ],
            [
                'id'    => 46,
                'title' => 'answer_delete',
            ],
            [
                'id'    => 47,
                'title' => 'answer_access',
            ],
            [
                'id'    => 48,
                'title' => 'profile_password_edit',
            ],
        ];

        Permission::insert($permissions);
    }
}
