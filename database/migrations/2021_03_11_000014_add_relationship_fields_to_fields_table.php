<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToFieldsTable extends Migration
{
    public function up()
    {
        Schema::table('fields', function (Blueprint $table) {
            $table->unsignedBigInteger('form_id')->nullable();
            $table->foreign('form_id', 'form_fk_3393278')->references('id')->on('forms');
            $table->unsignedBigInteger('step_id')->nullable();
            $table->foreign('step_id', 'step_fk_3393279')->references('id')->on('steps');
        });
    }
}
