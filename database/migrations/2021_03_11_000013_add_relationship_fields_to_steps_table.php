<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToStepsTable extends Migration
{
    public function up()
    {
        Schema::table('steps', function (Blueprint $table) {
            $table->unsignedBigInteger('form_id');
            $table->foreign('form_id', 'form_fk_3393283')->references('id')->on('forms');
        });
    }
}
