<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFormsTable extends Migration
{
    public function up()
    {
        Schema::create('forms', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('form_name');
            $table->longText('description')->nullable();
            $table->string('youtube_url')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }
}
