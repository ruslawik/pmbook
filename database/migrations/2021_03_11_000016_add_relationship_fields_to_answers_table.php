<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToAnswersTable extends Migration
{
    public function up()
    {
        Schema::table('answers', function (Blueprint $table) {
            $table->unsignedBigInteger('field_id');
            $table->foreign('field_id', 'field_fk_3393703')->references('id')->on('fields');
            $table->unsignedBigInteger('gen_doc_id');
            $table->foreign('gen_doc_id', 'gen_doc_fk_3393708')->references('id')->on('gen_docs');
        });
    }
}
