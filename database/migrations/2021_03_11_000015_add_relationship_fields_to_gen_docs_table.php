<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToGenDocsTable extends Migration
{
    public function up()
    {
        Schema::table('gen_docs', function (Blueprint $table) {
            $table->unsignedBigInteger('form_id');
            $table->foreign('form_id', 'form_fk_3393609')->references('id')->on('forms');
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id', 'user_fk_3393620')->references('id')->on('users');
        });
    }
}
